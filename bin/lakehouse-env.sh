#!/usr/bin/env bash
#
# Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
# Lakehouse is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#          http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
#

export LAKEHOUSE_HOME="${LAKEHOUSE_HOME:-"$(cd "$(dirname "$0")"/.. || exit; pwd)"}"
export LAKEHOUSE_JAVA_OPTS="-Xms2g -Xmx2g -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=./logs -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:./logs/lakehouse-gc-%t.log"

export LAKEHOUSE_CONF_DIR="${LAKEHOUSE_CONF_DIR:-"${LAKEHOUSE_HOME}/conf"}"
export LAKEHOUSE_LOG_DIR="${LAKEHOUSE_LOG_DIR:-"${LAKEHOUSE_HOME}/logs"}"
export LAKEHOUSE_PID_DIR="${LAKEHOUSE_PID_DIR:-"${LAKEHOUSE_HOME}/pid"}"