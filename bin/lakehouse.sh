#!/usr/bin/env bash
#
# Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
# Lakehouse is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#          http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
#

BINDIR=$(dirname "$0")
LAKEHOUSE_HOME=$(
  cd $BINDIR/..
  pwd
)

APP_NAME="lakehouse-api"
LAKEHOUSE_STOP_TIMEOUT=${LAKEHOUSE_STOP_TIMEOUT:-30}

# Check env and load
if [ -f "$LAKEHOUSE_HOME/bin/lakehouse-env.sh" ]; then
  . "$LAKEHOUSE_HOME/bin/lakehouse-env.sh"
fi

# Check for the java to use
if [[ -z $JAVA_HOME ]]; then
  JAVA=$(which java)
  if [ $? = 0 ]; then
    echo "JAVA_HOME not set, using java from PATH. ($JAVA)"
  else
    echo "Error: JAVA_HOME not set, and no java executable found in $PATH." 1>&2
    exit 1
  fi
else
  JAVA=$JAVA_HOME/bin/java
fi

lakehouse_help() {
  cat <<EOF
Usage: ${APP_NAME} <command>
where command is one of :
    start (-front)  Run a ${APP_NAME} service (at the front desk)
    stop (-force)   Stop the ${APP_NAME} service (forcibly)
    status          Check the ${APP_NAME} service process status
EOF
}

# if no args specified, show usage
if [ $# = 0 ]; then
  lakehouse_help
  exit 1
fi

CLASS="com.chinamobile.cmss.lakehouse.api.LakehouseApiServer"

LAKEHOUSE_JAR_DIR="$LAKEHOUSE_HOME/libs"
LAKEHOUSE_CLASSPATH="${LAKEHOUSE_JAR_DIR}/*:${LAKEHOUSE_CONF_DIR}"

CMD="${JAVA} ${LAKEHOUSE_JAVA_OPTS} -cp ${LAKEHOUSE_CLASSPATH} $CLASS"
PID="${LAKEHOUSE_PID_DIR}/lakehouse-api-$USER.pid"

startStop=$1
shift

daemon=true
forceStop=false

start_server() {

  if [ -f $PID ]; then
    if kill -0 $(cat $PID) >/dev/null 2>&1; then
      echo "${APP_NAME} is running as PID $(cat $PID). stop it first."
      exit 1
    fi
  fi
  echo "Starting ${APP_NAME} ..."
  if [ $daemon == true ]; then
    exec $CMD >$LAKEHOUSE_LOG_DIR/${APP_NAME}.out 2>&1 &
    echo $! >$PID
  else
    exec $CMD
  fi
  if [ -f $PID ]; then
    TARGET_PID=$(cat "$PID")
    RUN_PID=$(ps -ef |grep -v grep | grep "${APP_NAME}" | grep "$TARGET_PID"|awk '{print $2}')
    if [ "$TARGET_PID" == "$RUN_PID" ]; then
      echo "Starting ${APP_NAME} completed, running as PID $TARGET_PID."
    fi
  fi
}

stop_server() {

  if [ -f $PID ]; then
    TARGET_PID=$(cat $PID)
    if kill -0 $TARGET_PID >/dev/null 2>&1; then
      echo "Stopping ${APP_NAME} ..."
      kill $TARGET_PID

      count=0
      while ps -p $TARGET_PID >/dev/null; do
        echo "Shutdown is in progress... Please wait..."
        sleep 1
        count=$(expr $count + 1)

        if [ "$count" = "$LAKEHOUSE_STOP_TIMEOUT" ]; then
          break
        fi
      done

      if [ "$count" != "$LAKEHOUSE_STOP_TIMEOUT" ]; then
        echo "Shutdown completed."
      fi

      if kill -0 $TARGET_PID >/dev/null 2>&1; then
        fileName=$LAKEHOUSE_LOG_DIR/${APP_NAME}-jstack.log
        $JAVA_HOME/bin/jstack $TARGET_PID >$fileName
        echo "Thread dumps are taken for analysis at $fileName"
        if [ $forceStop == true ]; then
          echo "Stopping ${APP_NAME} forcibly."
          kill -9 $TARGET_PID >/dev/null 2>&1
          echo "Stop the process success."
        else
          echo "WARNNING: ${APP_NAME} is not stopped completely."
          exit 1
        fi
      fi
    else
      echo "no ${APP_NAME} to stop"
    fi
    : >"$PID"
  else
    echo "no ${APP_NAME} to stop"
  fi
}

check_status() {
  status='stopped'
  if [ -f $PID ]; then
    TARGET_PID=$(cat "$PID")
    RUN_PID=$(ps -ef |grep -v grep | grep "${APP_NAME}" | grep "$TARGET_PID"|awk '{print $2}')
    if [ "$TARGET_PID" == "$RUN_PID" ]; then
      status='running'
      echo "${APP_NAME} is $status as PID $TARGET_PID."
    else
      status='stopped'
      echo "${APP_NAME} is $status."
    fi
  fi

}

case "$startStop" in
  'start')
    while [ -n "$1" ]; do
      case "$1" in
      -front)
        daemon=false
        shift
        ;;
      *)
        lakehouse_help
        exit 1
        ;;
      esac
    done
    start_server
    ;;
  'stop')
    while [ -n "$1" ]; do
      case "$1" in
      -force)
        forceStop=true
        shift
        ;;
      *)
        lakehouse_help
        exit 1
        ;;
      esac
    done
    stop_server
    ;;
  'status')
    check_status
    ;;
  *)
    lakehouse_help
    exit 1
    ;;
esac