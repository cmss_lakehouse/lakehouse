/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.deploy.cluster.strategy;

import com.chinamobile.cmss.lakehouse.common.annotation.ClusterTypeAnno;
import com.chinamobile.cmss.lakehouse.common.dto.LakehouseCreateRequest;
import com.chinamobile.cmss.lakehouse.common.enums.ClusterTypeEnum;
import com.chinamobile.cmss.lakehouse.common.exception.BaseException;
import com.chinamobile.cmss.lakehouse.core.handler.K8sDeployHandler;

import java.util.List;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ClusterDeployStrategyContext {

    @Autowired
    protected K8sDeployHandler deployHandler;
    @Resource
    private List<ClusterDeployStrategy> clusterDeployStrategies;

    public void deploy(LakehouseCreateRequest request) {
        log.info("start deploy lakehouse in k8s:{}", request.getInstance());
        ClusterTypeEnum clusterType = request.getType();
        for (ClusterDeployStrategy clusterDeployStrategy : clusterDeployStrategies) {
            ClusterTypeAnno clusterTypeAnno =
                clusterDeployStrategy.getClass().getAnnotation(ClusterTypeAnno.class);
            if (clusterTypeAnno != null && clusterTypeAnno.value() == clusterType) {
                clusterDeployStrategy.deploy(request);
                return;
            }
        }
        log.info("cluster:{} deploy success!", request.getInstance());
        throw new BaseException("Can't find deploy strategy for cluster: " + clusterType.name());
    }

}