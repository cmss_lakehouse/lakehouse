/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.deploy.component.instance.hive;

import com.chinamobile.cmss.lakehouse.core.config.ConfigOption;

public class HiveComponentOptions {

    public static final ConfigOption<Integer> HIVE_REPLICA =
        ConfigOption.key("cluster.component.hive.replica").defaultValue(2);

    public static final ConfigOption<String> HIVE_WAREHOUSE_DIR =
        ConfigOption.key("hive.metastore.warehouse.dir").defaultValue("/hive/warehouse/data");

    public static final ConfigOption<String> HIVE_DEFAULT_FS =
        ConfigOption.key("fs.defaultFS").defaultValue("s3a://test");

    public static final ConfigOption<String> HIVE_S3_ENDPOINT =
        ConfigOption.key("fs.s3a.endpoint").defaultValue("http://localhost");

    public static final ConfigOption<String> HIVE_S3_ACCESS_KEY =
        ConfigOption.key("fs.s3a.access.key").defaultValue("5WSALBJLL3XV6U33DR26");

    public static final ConfigOption<String> HIVE_S3_SECRET_KEY =
        ConfigOption.key("fs.s3a.secret.key").defaultValue("JY6ilRHBKk5UR8fDhxAzGBwEjweoEFi56pfwdPME");

}
