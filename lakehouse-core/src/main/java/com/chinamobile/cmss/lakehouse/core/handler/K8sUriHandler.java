/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.handler;

import static com.chinamobile.cmss.lakehouse.common.kubernetes.K8sModelConstant.CLUSTER_NAMESPACE_PREFIX;
import static com.chinamobile.cmss.lakehouse.common.kubernetes.K8sModelConstant.COMMON_NAMESPACE_SUFFIX;
import static com.chinamobile.cmss.lakehouse.common.kubernetes.K8sModelConstant.NAMESPACE_SEPARATOR;

import com.chinamobile.cmss.lakehouse.core.config.KubernetesConfiguration;
import com.chinamobile.cmss.lakehouse.core.deploy.component.instance.hive.HiveComponentDescriptor;
import com.chinamobile.cmss.lakehouse.core.deploy.component.instance.meta.MetaComponentDescriptor;
import com.chinamobile.cmss.lakehouse.core.deploy.component.instance.yunikorn.YuniKornComponentDescriptor;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class K8sUriHandler {

    private static final String COMMON_NAMESPACE = CLUSTER_NAMESPACE_PREFIX + NAMESPACE_SEPARATOR + COMMON_NAMESPACE_SUFFIX;

    @Resource
    private HiveComponentDescriptor hiveDescriptor;

    @Resource
    private MetaComponentDescriptor metaDescriptor;

    @Resource
    private YuniKornComponentDescriptor yunikornDescriptor;

    public String getInternalHiveUrl() {
        return "jdbc:hive2://" + hiveDescriptor.k8sInternalService(COMMON_NAMESPACE)
            + "/default?hive.metastore.uris="
            + getInternalMetaUrl();
    }

    public String getExternalHiveUrl() {
        return "jdbc:hive2://" + hiveDescriptor.k8sExternalService(COMMON_NAMESPACE)
            + "/default?hive.metastore.uris="
            + getExternalMetaUrl();
    }

    public String getInternalMetaUrl() {
        return "thrift://" + metaDescriptor.k8sInternalService(COMMON_NAMESPACE);
    }

    public String getExternalMetaUrl() {
        return "thrift://" + metaDescriptor.k8sExternalService(COMMON_NAMESPACE);
    }

    public String getYunikornQueuesUrl() {
        return "http://" + yunikornService() + "/ws/v1/queues";
    }

    public String getYunikornConfigUrl() {
        return "http://" + yunikornService() + "/ws/v1/config";
    }

    public String yunikornService() {
        return KubernetesConfiguration.exposeServiceEnabled ? yunikornDescriptor.k8sExternalService(COMMON_NAMESPACE)
            : yunikornDescriptor.k8sInternalService(COMMON_NAMESPACE);
    }

    public String getRouterUri(String namespace) {
        return String.format("hdfs://router-headless.%s.svc.cluster.local:8888", namespace);
    }

}