/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.client;

import java.util.concurrent.CompletableFuture;

public interface InformerFactory {

    /**
     * Get or add namespace informer
     *
     * @return
     */
    CompletableFuture<Void> getOrAddNamespaceInformer();

    /**
     * Get or add clusterRoleBinding informer
     *
     * @return
     */
    CompletableFuture<Void> getOrAddClusterRoleBindingInformer();

    /**
     * Get or add service informer in a namespace
     *
     * @return
     */
    CompletableFuture<Void> getOrAddServiceInformer();

    /**
     * Get or add statefulset informer in a namespace
     *
     * @return
     */
    CompletableFuture<Void> getOrAddStatefulSetInformer();

    /**
     * Get or add deployment informer in a namespace
     *
     * @return
     */
    CompletableFuture<Void> getOrAddDeploymentInformer();

    /**
     * Get or add configmap informer in a namespace
     *
     * @return
     */
    CompletableFuture<Void> getOrAddConfigMapInformer();

    /**
     * Shutdown all informers
     */
    void shutdown();
}
