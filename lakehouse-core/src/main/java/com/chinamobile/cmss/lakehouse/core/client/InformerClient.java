/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.client;

import com.chinamobile.cmss.lakehouse.core.client.impl.InformerFactoryImpl;

import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.V1ClusterRoleBinding;
import io.kubernetes.client.openapi.models.V1ConfigMap;
import io.kubernetes.client.openapi.models.V1Deployment;
import io.kubernetes.client.openapi.models.V1Namespace;
import io.kubernetes.client.openapi.models.V1Service;
import io.kubernetes.client.openapi.models.V1StatefulSet;

public interface InformerClient {
    /**
     * create namespace
     *
     * @param namespace to be created
     * @return null means action Exception
     * @throws InformerException
     */
    V1Namespace createNamespace(V1Namespace namespace) throws InformerException;

    /**
     * delete namespace
     *
     * @param namespace to be deleted
     * @return null means action Exception
     * @throws InformerException
     */
    V1Namespace deleteNamespace(String namespace) throws InformerException;

    /**
     * create clusterRoleBinding
     *
     * @param clusterRoleBinding
     * @return
     * @throws InformerException
     */
    V1ClusterRoleBinding createClusterRoleBinding(V1ClusterRoleBinding clusterRoleBinding)
        throws InformerException;

    /**
     * delete clusterRoleBinding
     *
     * @param clusterRoleBinding
     * @return
     * @throws InformerException
     */
    V1ClusterRoleBinding deleteClusterRoleBinding(String clusterRoleBinding) throws InformerException;

    /**
     * create service in a namespace
     *
     * @param namespace name
     * @param service   to be created
     * @return null means action Exception
     * @throws InformerException
     */
    V1Service createService(String namespace, V1Service service) throws InformerException;

    /**
     * delete service in a namespace
     *
     * @param namespace   name
     * @param serviceName to be deleted
     * @return null means action Exception
     * @throws InformerException
     */
    V1Service deleteService(String namespace, String serviceName) throws InformerException;

    /**
     * create StatefulSet in a namespace
     *
     * @param namespace   name
     * @param statefulSet to be created
     * @return null means action Exception
     * @throws InformerException
     */
    V1StatefulSet createStatefulSet(String namespace, V1StatefulSet statefulSet)
        throws InformerException;

    /**
     * create Deployment in a namespace
     *
     * @param namespace  name
     * @param deployment to be created
     * @return null means action Exception
     * @throws InformerException
     */
    V1Deployment createDeployment(String namespace, V1Deployment deployment) throws InformerException;

    /**
     * patch Deployment in a namespace
     *
     * @param namespace      name
     * @param jsonPatch      patch
     * @param deploymentName deployment name
     * @return null means action Exception
     * @throws InformerException
     */
    V1Deployment patchDeployment(String namespace, String jsonPatch, String deploymentName) throws InformerException;

    /**
     * patch StatefulSet in a namespace
     *
     * @param namespace
     * @param jsonPatch
     * @param statefulSetName
     * @return
     * @throws InformerException
     */
    boolean patchStatefulSet(String namespace, String jsonPatch, String statefulSetName) throws ApiException;

    /**
     * delete StatefulSet in a namespace
     *
     * @param namespace       name
     * @param statefulSetName to be deleted
     * @return null means action Exception
     * @throws InformerException
     */
    V1StatefulSet deleteStatefulSet(String namespace, String statefulSetName) throws InformerException;

    /**
     * delete Deployment in a namespace
     *
     * @param namespace      name
     * @param deploymentName to be deleted
     * @return null means action Exception
     * @throws InformerException
     */
    V1Deployment deleteDeployment(String namespace, String deploymentName) throws InformerException;

    /**
     * create configMap in a namespace
     *
     * @param namespace name
     * @param configMap to be created
     * @return null means action Exception
     * @throws InformerException
     */
    V1ConfigMap createConfigMap(String namespace, V1ConfigMap configMap) throws InformerException;

    /**
     * delete configMap in a namespace
     *
     * @param namespace     name
     * @param configMapName to be deleted
     * @return null means action Exception
     * @throws InformerException
     */
    V1ConfigMap deleteConfigMap(String namespace, String configMapName) throws InformerException;

    /**
     * get informer factory
     *
     * @return
     */
    InformerFactoryImpl getInformerFactory();

    /**
     * check status of three type resources: Namespace, StatefulSet, Pvc
     *
     * @param namespace
     * @return false if one of the resources is not healthy
     * @throws ApiException
     */
    boolean checkResourceHealthStatus(String namespace) throws ApiException;
}
