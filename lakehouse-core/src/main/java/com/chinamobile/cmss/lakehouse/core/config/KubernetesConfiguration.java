/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.config;

import com.chinamobile.cmss.lakehouse.common.Constants;
import com.chinamobile.cmss.lakehouse.common.exception.BaseException;
import com.chinamobile.cmss.lakehouse.common.utils.FileUtils;
import com.chinamobile.cmss.lakehouse.common.utils.PropertyUtils;

import java.io.IOException;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KubernetesConfiguration {

    private static KubeConfig kubeConfig;

    @Autowired
    private KubernetesConfiguration(KubeConfig kubeConfig) {
        KubernetesConfiguration.kubeConfig = kubeConfig;
    }

    public static final String hiveServerSitePath = Constants.HIVE_SERVER_SITE_PATH;
    public static final String hiveMetaSitePath = Constants.HIVE_META_SITE_PATH;

    public static final String hiveYamlTemplatePath = Constants.TEMPLATE_HIVE_YAML_PATH;
    public static final String metaYamlTemplatePath = Constants.TEMPLATE_META_YAML_PATH;
    public static final String yunikornYamlTemplatePath = Constants.TEMPLATE_YUNIKORN_YAML_PATH;

    public static final String hiveImage = PropertyUtils.getString(Constants.KUBERNETES_IMAGE_HIVE);
    public static final String sparkImage = PropertyUtils.getString(Constants.KUBERNETES_IMAGE_SPARK);
    public static final String yunikornSchedulerImage = PropertyUtils.getString(Constants.KUBERNETES_IMAGE_YUNIKORN_SCHEDULER);
    public static final String yunikornWebImage = PropertyUtils.getString(Constants.KUBERNETES_IMAGE_YUNIKORN_WEB);

    public static final int hiveReplicas = PropertyUtils.getInt(Constants.KUBERNETES_RESOURCE_REPLICAS_HIVE);
    public static final int metaReplicas = PropertyUtils.getInt(Constants.KUBERNETES_RESOURCE_REPLICAS_META);
    public static final int yunikornReplicas = PropertyUtils.getInt(Constants.KUBERNETES_RESOURCE_REPLICAS_YUNIKORN);

    public static final double containerVcore = PropertyUtils.getDouble(Constants.KUBERNETES_RESOURCE_CONTAINER_VCORE, 2.0);
    public static final int hiveHeap = PropertyUtils.getInt(Constants.KUBERNETES_RESOURCE_CONTAINER_HEAP_HIVE);
    public static final int metaHeap = PropertyUtils.getInt(Constants.KUBERNETES_RESOURCE_CONTAINER_HEAP_META);
    public static final int yunikornHeap = PropertyUtils.getInt(Constants.KUBERNETES_RESOURCE_CONTAINER_HEAP_YUNIKORN);
    public static final boolean exposeServiceEnabled = PropertyUtils.getBoolean(Constants.KUBERNETES_RESOURCE_EXPOSE_SERVICE_ENABLED, false);

    public static final long informerWaitMs = PropertyUtils.getLong(Constants.KUBERNETES_INFORMER_WAIT_MS);
    public static final int informerSyncRetryNumber = PropertyUtils.getInt(Constants.KUBERNETES_INFORMER_SYNC_RETRIES_NUMBER);
    public static final long informerSyncRetryIntervalMs = PropertyUtils.getLong(Constants.KUBERNETES_INFORMER_SYNC_RETRIES_INTERVAL_MS);
    public static final int informerCheckRetryNumber = PropertyUtils.getInt(Constants.KUBERNETES_INFORMER_CHECK_RETRIES_NUMBER);
    public static final long informerCheckRetryIntervalMs = PropertyUtils.getLong(Constants.KUBERNETES_INFORMER_CHECK_RETRIES_INTERVAL_MS);

    public static final double cpuOverRatio = PropertyUtils.getDouble(Constants.KUBERNETES_RATIO_CPU_OVER, 1.0);
    public static final double memoryOverRatio = PropertyUtils.getDouble(Constants.KUBERNETES_RATIO_MEMORY_OVER, 1.25);

    /**
     * meta-site.xml
     */
    public static SimpleHadoopConfiguration getMetaSiteContent() {
        SimpleHadoopConfiguration config = new SimpleHadoopConfiguration();
        config.loadResource(FileUtils.readToStream(hiveMetaSitePath));
        return config;
    }

    /**
     * hive-site.xml
     */
    public static SimpleHadoopConfiguration getHiveSiteContent() {
        SimpleHadoopConfiguration config = new SimpleHadoopConfiguration();
        config.loadResource(FileUtils.readToStream(hiveServerSitePath));
        return config;
    }

    public static String getHiveYamlTemplate() {
        try {
            return FileUtils.readToString(hiveYamlTemplatePath);
        } catch (IOException e) {
            throw new BaseException(e);
        }
    }

    public static String getMetaYamlTemplate() {
        try {
            return FileUtils.readToString(metaYamlTemplatePath);
        } catch (IOException e) {
            throw new BaseException(e);
        }
    }

    public static String getYunikornYamlTemplate() {
        try {
            return FileUtils.readToString(yunikornYamlTemplatePath);
        } catch (IOException e) {
            throw new BaseException(e);
        }
    }

    public static Map<String, String> getNodeSelector() {
        String[] nodeLabels = kubeConfig.getNodeSelector().split("=");
        return ImmutableMap.of(nodeLabels[0], nodeLabels[1]);
    }

    public static String getK8sDefaultClusterInfo() {
        return "k8s://" + kubeConfig.getApiServer();
    }

}