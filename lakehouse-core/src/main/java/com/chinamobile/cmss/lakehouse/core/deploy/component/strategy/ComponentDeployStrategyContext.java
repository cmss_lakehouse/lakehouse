/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.deploy.component.strategy;

import com.chinamobile.cmss.lakehouse.common.annotation.ComponentTypeAnno;
import com.chinamobile.cmss.lakehouse.common.enums.ComponentTypeEnum;
import com.chinamobile.cmss.lakehouse.common.exception.BaseException;
import com.chinamobile.cmss.lakehouse.core.config.ComponentConfiguration;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

@Service
public class ComponentDeployStrategyContext {

    @Resource
    private List<ComponentDeployStrategy> componentDeployStrategies;

    public void deploy(String namespace, ComponentTypeEnum type, String clusterName,
                       ComponentConfiguration config) {

        for (ComponentDeployStrategy componentDeployStrategy : componentDeployStrategies) {
            ComponentTypeAnno componentTypeAnno =
                componentDeployStrategy.getClass().getAnnotation(ComponentTypeAnno.class);
            if (componentTypeAnno != null && componentTypeAnno.value() == type) {
                componentDeployStrategy.deploy(namespace, clusterName, config);
                return;
            }
        }

        throw new BaseException("Can't find deploy strategy for component: " + type.name());
    }

}