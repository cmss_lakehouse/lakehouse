/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.deploy.cluster.instance;

import com.chinamobile.cmss.lakehouse.common.annotation.ClusterTypeAnno;
import com.chinamobile.cmss.lakehouse.common.dto.LakehouseCreateRequest;
import com.chinamobile.cmss.lakehouse.common.enums.ClusterTypeEnum;
import com.chinamobile.cmss.lakehouse.core.deploy.cluster.descriptor.ClusterDescriptor;
import com.chinamobile.cmss.lakehouse.core.deploy.cluster.strategy.ClusterDeployStrategy;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@ClusterTypeAnno(ClusterTypeEnum.LAKEHOUSE)
public class LakehouseClusterDeployStrategy extends ClusterDeployStrategy {

    @Resource
    private LakehouseClusterDescriptor descriptor;

    @Override
    public void deploy(LakehouseCreateRequest request) {
        log.info("deploy lakehouse cluster to k8s");
        super.deploy(request);
    }

    @Override
    protected ClusterDescriptor descriptor() {
        return descriptor;
    }

}
