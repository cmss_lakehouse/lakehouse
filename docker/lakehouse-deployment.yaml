#
# Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
# Lakehouse is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#          http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
#

# Usage:
# 1. Create a namespace (e.g. lakehouse-management):
#   $ kubectl create ns lakehouse-management
#
# 2. Edit parameters in application.yaml with your environment configurations (such as: mysql url, kubernetes api-server url, redis url .etc):
#   $ vi ${LAKEHOUSE_HOME}/conf/application.yaml
#
# 3. Create config map:
#   $ kubectl create cm api-config --from-file=${LAKEHOUSE_HOME}/conf/application.yaml -n lakehouse-management
#
# 4. Edit parameters in lakehouse-deployment.yaml with your environment configurations (such as: image, replicas, node selector .etc):
#   $ vi ${LAKEHOUSE_HOME}/docker/lakehouse-deployment.yaml
#
# 5. Start deployment with exposing port by NodePort:
#   $ kubectl apply -f ${LAKEHOUSE_HOME}/docker/lakehouse-deployment.yaml -n lakehouse-management
#
# 6. Change config:
#   $ kubectl edit cm api-config -n lakehouse-management
#
# 7. Restart deployment:
#   $ kubectl rollout restart api-deployment -n lakehouse-management

---
apiVersion: v1
kind: Service
metadata:
  name: api-service
  labels:
    app: lakehouse
    component: api
spec:
  selector:
    app: lakehouse
    component: api
  ports:
    - nodePort: 30086
      port: 19086
      protocol: TCP
  type: NodePort
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: api-deployment
  labels:
    app: lakehouse
    component: api
spec:
  selector:
    matchLabels:
      app: lakehouse
      component: api
  replicas: 1
  template:
    metadata:
      labels:
        app: lakehouse
        component: api
    spec:
      terminationGracePeriodSeconds: 10
      nodeSelector:
        app: lakehouse
      containers:
        - name: api
          image: cis-hub-huadong-4.cmecloud.cn/tilake/lakehouse-api:1.0.0-SNAPSHOT
          imagePullPolicy: Always
          resources:
            limits:
              cpu: "2"
              memory: 4Gi
            requests:
              cpu: "1"
              memory: 2Gi
          command: ["sh", "-c"]
          args:
            - >
              exec bin/lakehouse.sh start -front;
          ports:
            - name: rpc
              containerPort: 19086
          volumeMounts:
            - name: config-volume
              subPath: application.yaml
              mountPath: /opt/lakehouse-api/conf/application.yaml
      volumes:
        - name: config-volume
          configMap:
            name: api-config