/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.engine.meta.crawler.constant

object ConfigPath {
  val USER = "user"
  val SOURCE_AK = "source.ak"
  val SOURCE_SK = "source.sk"
  val SOURCE_ENDPOINT = "source.endpoint"
  val SOURCE_PATH = "source.path"
  val SOURCE_RESOLVER = "source.resolver"
  val JOB_ID = "jobId"
  val TARGET_INSTANCE = "target.instance"
  val TARGET_PREFIX = "target.tablePrefix"
  val METADATA_URL = "metadataUrl"
  val MAX_COLUMN = "source.maxColumn"
}
