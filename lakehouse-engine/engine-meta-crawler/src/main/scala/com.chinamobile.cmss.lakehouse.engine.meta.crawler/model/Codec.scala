/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.engine.meta.crawler.model

import io.circe.generic.semiauto._
import io.circe.{Encoder, Json}
import org.apache.commons.lang3.StringUtils

object Codec extends Encoders

trait Encoders {

  import com.chinamobile.cmss.lakehouse.engine.meta.crawler.Encoders._
  implicit val syncTableEncoder: Encoder[SyncTable] = deriveEncoder
  implicit val tableEncoder: Encoder[Table] = deriveEncoder
  implicit val columnInfoEncoder: Encoder[ColumnInfo] = deriveEncoder
  implicit val partitionValueEncoder: Encoder[PartitionValue] = deriveEncoder

  implicit val ColumnType: Encoder[ColumnType] =
    Encoder.instance { tpe =>
      tpe match {
        case t: DecimalType =>
          Json.fromString(s"DecimalType_${t.precision}_${t.scala}")
        case _ =>
          Json.fromString(
            StringUtils.removeEnd(tpe.getClass.getSimpleName, "$")
          )
      }
    }

}
