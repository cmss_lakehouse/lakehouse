/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

const theme = {
  light: '浅色',
  dark: '深色'
}

const login = {
  username_tips: '请输入用户名',
  password_tips: '请输入密码',
  login: '立即登录'
}

const common = {
  positiveText: '确定',
  negativeText: '取消',
  format: '格式化',
  excution: '执行',
  instance: '实例',
  refresh: '刷新',
  search: '搜索',
  delete: '删除',
  create: '创建',
  action: '操作',
  reset: '重置',
  min: '分钟',
  hour: '小时',
  day: '日',
  week: '周',
  month: '月',
  help_center: '帮助中心'
}

const userDropdown = {
  profile: '用户信息',
  password: '密码管理',
  logout: '退出登录'
}

const menu = {
  home: '首页',
  instance: '实例列表',
  synchronization: '数据同步',
  explore: '数据探索',
  sqlconsole: 'SQL Console',
  user: '用户管理',
  data_manage: '数据管理',
  database: '数据库',
  directory: '数据目录',
  discovery: '元数据发现'
}

const home = {
  welcome: '欢迎',
  instance_number: '实例个数',
  sets: '个',
  view_instance_list: '查看实例列表',
  today_job_number: '今日提交作业数',
  view_job_monitor: '查看作业监控',
  use_guide: '使用向导',
  initialize_instance: '初始化实例',
  create_instance: '创建实例',
  create_instance_desc: '选择计算引擎，创建实例',
  bulid_datalake: '构建数据湖/仓',
  create_database: '创建数据库',
  create_database_desc: '创建一个数据库，用于目标数据的管理',
  create_now: '立即创建',
  data_sync: '数据同步',
  data_sync_desc: '新数据入湖/仓场景，数据连接-任务创建-任务监控',
  one_click_sync: '一键同步',
  metadata_discovery: '元数据发现',
  metadata_discovery_desc: '对象存储存量数据发现场景，任务创建-任务监控',
  discovry_now: '立即发现',
  data_directory: '数据目录',
  data_directory_desc: '查看构建的数据湖/仓数据目录，并进行管理',
  view_data_directory: '查看数据目录',
  data_explore: '数据探索',
  sql_console: 'SQL Console',
  sql_console_desc: '使用Spark SQL进行数据分析'
}

const pagination = {
  total: '共',
  record: '条记录'
}

const instance = {
  instance_list: '实例列表',
  instance_name: '实例名称',
  engine_type: '计算引擎',
  compute_resource_size: '计算资源',
  status: '状态',
  duration: '累计运行时长',
  create_user: '创建人',
  create_time: '创建时间',
  create: '创建实例',
  manage: '管理',
  release: '释放',
  delete_tips: '是否释放实例',
  delete_tips_desc: '实例下所有数据将被清空且不可恢复，请谨慎操作。',
  create_modal: {
    size: '规格',
    scene: '适用场景',
    cancel: '取消',
    submit: '完成',
    instance_name_tips: '请输入实例名称',
    instance_name_format_tips:
      '仅允许字母开头，长度限制4～32个字符，可包含小写字母、数字、下划线',
    compute_resource_tips: '计算资源的大小决定了查询性能，您可以独享该部分资源'
  },
  base_info: {
    base_info: '基本信息',
    instance_name: '实例名称',
    instance_id: '实例ID',
    status: '状态',
    engine_type: '计算引擎',
    compute_resource_size: '计算资源',
    create_user: '创建人',
    create_time: '创建时间'
  },
  system_config: {
    system_config: '系统设置',
    system_config_tips1:
      '1. 新值不影响当前正在运行的作业，将在新作业开始执行时生效；',
    system_config_tips2:
      '2.减小作业最大并发数可以提高单个作业执行效率，增大则可能导致作业执行失败，请谨慎操作。',
    max_parallelize_num: '作业最大并发数'
  },
  status_types: {
    running: '正常',
    creating: '创建中',
    create_failed: '创建失败',
    error: '异常',
    resizing: '变更中',
    accepted: '初始化中'
  },
  size_types: {
    small: '小型（16CU）',
    medium: '中型（64CU）',
    large: '大型（128CU）',
    xlarge: 'X-大型（256CU）'
  },
  size_desc: {
    small: '适合初次订购、数据量较小、性能要求低的场景',
    medium: '规格适中，适合数据量在30TB以内的场景',
    large: '中大型企业推荐配置，适合数据量在30-50TB的场景',
    xlarge: '高性能配置，适合数据量在50-100TB的场景'
  }
}

const synchronization = {
  job_name: '任务名称',
  job_id: '任务ID',
  schedule_status: '调度状态',
  create_user: '创建人',
  create_time: '创建时间',
  job_type: '任务类型',
  create_form: {
    step1_title: '任务类型',
    step2_title: '基本配置信息',
    step3_title: '源和目标配置',
    step4_title: '任务配置',
    step5_title: '流程确认',
    next_step: '下一步',
    RDB_ALL_label: '关系型数据库离线同步',
    RDB_ALL_desc:
      '将指定的关系型数据库整库或部分表一次性或周期性同步到目标内置存储或对象存储中，建议在业务低峰期执行，目前支持的数据库包括数据库MySQL',
    MQ_REALTIME_label: '消息队列实时同步',
    MQ_REALTIME_desc:
      '将消息队列如Kafka、Pulsar中的数据实时同步到目标内置存储或对象存储中，目前仅支持消息队列Kafka',
    job_desc: '任务描述',
    data_source_config: '数据源配置',
    connector_group: '数据连接分类',
    connector_type: '连接器类型',
    connector_id: '数据连接名称/ID',
    database_name: '数据库名称',
    table: '表',
    table_name: '表名',
    data_target_config: '数据目标配置',
    table_prefix: '表名称前缀',
    store_type: '存储类型',
    connector_path: '连接地址',
    ak: '访问标识（AK）',
    sk: '密钥（SK）',
    store_path: '存储路径',
    partition: '分区',
    schedule_model: '调度方式',
    once: '单次',
    cycle: '周期',
    schedule_time: '开始执行时间',
    duration_value: '调度周期',
    schedule_start_time: '调度开始时间',
    schedule_end_time: '调度结束时间',
    fail_action: '任务失败策略',
    show_advanced_config: '显示高级属性',
    hide_advanced_config: '隐藏高级属性',
    concurrence: '任务并发数',
    type: '类型',
    relational_database: '关系型数据库',
    all: '全部',
    multi: '多选'
  }
}

const database = {
  name: '数据库名称',
  table_count: '表数量',
  description: '描述',
  creator: '创建人',
  create_time: '创建时间',
  updator: '最近更新人',
  update_time: '最近更新时间',
  table: '表',
  create_database: '创建数据库',
  search_database: '按库名/表名搜索',
  null_database: '暂无数据库',
  create_database_tips:
    '您还可以创建48个数据库，数据库名称一旦创建完成则无法修改，请谨慎填写'
}

const directory = {
  table_name: '表名',
  database_name: '数据库名称',
  location: '位置',
  ref_task_type: '关联任务类型',
  ref_task_id: '关联任务执行ID',
  creator: '创建人',
  create_time: '创建时间',
  last_modifier: '最近更新人',
  last_modifier_time: '最近更新时间',
  search_data: '查询数据'
}

const user = {
  username: '用户名',
  password: '密码',
  description: '描述',
  addUser: '新增用户',
  delete_tips: '是否删除用户',
  cancel: '取消',
  reset: '重置密码',
  addModal: {
    username: '用户名',
    password: '密码',
    passwordConfirm: '确认密码',
    description: '描述',
    cancel: '取消',
    submit: '确定',
    username_tips: '请输入用户名',
    username_format_tips:
      '长度限制2～32个字符，仅支持字母、数字、下划线、中划线用户名一旦提交不可更改，请谨慎填写',
    password_tips: '请输入密码',
    password_format_tips:
      '长度限制8～20个字符，包含大写字母、小写字母、数字、特殊字符，每种至少一个，特殊字符为!@#$%^&*()_+-=',
    confirm_password_tips: '请输入确认密码',
    two_password_entries_are_inconsistent: '两次密码输入不一致'
  },
  resetModal: {
    password_tips: '请输入密码',
    password_format_tips:
      '长度限制8～20个字符，包含大写字母、小写字母、数字、特殊字符，每种至少一个，特殊字符为!@#$%^&*()_+-=',
    confirm_password_tips: '请输入确认密码',
    two_password_entries_are_inconsistent: '两次密码输入不一致'
  }
}

const explore = {
  removeTabTip: '是否确认关闭当前查询窗口?',
  sqlEditorTip: 'SQL并行执行，如果SQL语句存在依赖关系，请分开执行',
  exec_history_list_title: '仅展示近30天执行历史',
  exec_history_taskId: '作业ID',
  exec_history_submitTime: '执行开始时间',
  exec_history_sqlContent: '执行语句',
  exec_history_status: '执行状态',
  exec_history_runTime: '运行时长(s)',
  exec_history_search: '按库名/表名搜索',
  exec_result_exec_tip: '当前语句正在执行',
  exec_result_abort_exec_tip: 'SQL语句已经被中断执行',
  exec_result_abort_tip: '中断',
  exec_result_submit_failed: '提交失败',
  exec_result_failed_msg: '错误信息: ',
  exec_result_execution_failed: '执行失败',
  exec_result_showlog: '查看日志',
  success_result_search_limit: '每次查询最多展示400行记录',
  success_result_run_success: '【消息】执行成功',
  success_result_search_tip: '按查询结果内容搜索',
  query_panel_grammar_url: '语法手册',
  db_not_created: '暂无数据库，请前往数据库创建',
  db_not_selected: '请选择数据库',
  db_table: '库/表',
  accepted_status: '已接收',
  submitted_status: '已提交',
  running_status: '执行中',
  submit_failed_status: '提交失败',
  cancelled_status: '已中断',
  failed_status: '执行失败',
  finished_status: '完成',
  lost_status: '丢失',
  unknown_status: '未知',
  return_database_page: '是否返回到数据目录页面？',
  return_database_page_tip: '返回后当前页面的操作内容不会被保留, 请确认返回！',
  execution_history: '执行历史'
}

export default {
  theme,
  login,
  common,
  userDropdown,
  menu,
  home,
  instance,
  database,
  directory,
  user,
  explore,
  synchronization,
  pagination
}
