/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { h, reactive, ref } from 'vue'
import { useI18n } from 'vue-i18n'
import { useRouter } from 'vue-router'
import { NButton, useDialog } from 'naive-ui'
import type { Router } from 'vue-router'
import {
  deleteDatabaseSingleReq,
  getDatabaseList
} from '@/service/modules/database'

export function useTable() {
  const { t } = useI18n()
  const router: Router = useRouter()
  const dialog = useDialog()

  const variables = reactive({
    columns: [],
    tableWidth: 500,
    tableData: [],
    checkedRowKeys: ref([]),
    page: ref(1),
    pageSize: ref(10),
    searchVal: ref(''),
    totalPage: ref(1),
    showModalRef: ref(false),
    statusRef: ref(0),
    row: {},
    loadingRef: ref(false)
  })

  const onHandleDelete = (row: any) => {
    const d = dialog.warning({
      content: `是否删除数据库${row.id}`,
      positiveText: t('common.positiveText'),
      onPositiveClick: () => {
        d.loading = true
        return new Promise((resolve) => {
          resolve
        })
      }
    })
    deleteDatabaseSingleReq(row.id)
  }

  const handleCheck = (rowKeys: any) => {
    variables.checkedRowKeys = rowKeys
  }

  const getList = async () => {
    if (variables.loadingRef) return
    variables.loadingRef = true

    const listRes = await getDatabaseList({
      pageNum: variables.page,
      pageSize: variables.pageSize,
      name: variables.searchVal
    })
    variables.loadingRef = false
    variables.tableData = listRes.entity.list
    variables.totalPage = listRes.totalPage
  }

  const handleToTable = (row: any) => {
    router.push(`/directory/${row.id}`)
  }

  const createColumns = (variables: any) => {
    variables.columns = [
      {
        type: 'selection'
      },
      {
        title: t('database.name'),
        key: 'name'
      },
      {
        title: t('database.table_count'),
        key: 'tableCount'
      },
      {
        title: t('database.description'),
        key: 'description'
      },
      {
        title: t('database.creator'),
        key: 'creator'
      },
      {
        title: t('database.create_time'),
        key: 'createTime'
      },
      {
        title: t('database.updator'),
        key: 'updator'
      },
      {
        title: t('database.update_time'),
        key: 'updateTime'
      },
      {
        title: t('common.action'),
        key: 'actions',
        render(row: any) {
          return h('span', null, [
            h(
              NButton,
              {
                text: true,
                type: 'info',
                strong: true,
                size: 'small',
                onClick: () => {
                  handleToTable(row)
                }
              },
              { default: () => t('database.table') }
            ),
            h(
              NButton,
              {
                text: true,
                type: 'info',
                strong: true,
                size: 'small',
                style: {
                  'margin-left': '10px'
                },
                onClick: () => {
                  onHandleDelete(row)
                }
              },
              { default: () => t('common.delete') }
            )
          ])
        }
      }
    ]
  }

  return {
    variables,
    createColumns,
    onHandleDelete,
    getList,
    handleCheck
  }
}
