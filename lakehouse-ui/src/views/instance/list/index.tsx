/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import {
  NButton,
  NDataTable,
  NIcon,
  NInput,
  NPagination,
  NSpace
} from 'naive-ui'
import { defineComponent, onMounted, toRefs, watch } from 'vue'
import { useI18n } from 'vue-i18n'
import styles from './index.module.scss'
import { useTable } from './use-table'
import { SearchOutlined } from '@vicons/antd'
import CreateModal from './components/create-modal'

const list = defineComponent({
  name: 'list',
  setup() {
    const { t } = useI18n()
    const { variables, createColumns, getList, changePage, changePageSize } =
      useTable()

    const clickCreate = () => {
      variables.showModalRef = true
    }

    const onCancelModal = () => {
      variables.showModalRef = false
    }

    const onConfirmModal = () => {
      variables.showModalRef = false
      getList()
    }

    onMounted(() => {
      createColumns(variables)
      getList()
    })

    watch(useI18n().locale, () => {
      createColumns(variables)
      getList()
    })

    return {
      t,
      clickCreate,
      onCancelModal,
      onConfirmModal,
      getList,
      changePage,
      changePageSize,
      ...toRefs(variables)
    }
  },
  render() {
    const {
      clickCreate,
      t,
      onCancelModal,
      showModalRef,
      loadingRef,
      getList,
      onConfirmModal,
      changePage,
      changePageSize
    } = this
    return (
      <div class={styles.container}>
        <div class={styles['table-container']}>
          <NSpace class={styles['table-header']} justify='space-between'>
            <div class={styles['header-buttons']}>
              <NButton type='primary' onClick={clickCreate}>
                {t('instance.create')}
              </NButton>
              <NButton onClick={getList} style='margin-left: 10px'>
                {t('common.refresh')}
              </NButton>
            </div>
            <div class={styles['header-search']}>
              <NInput v-model={[this.searchVal, 'value']} onInput={getList}>
                {{
                  prefix: () => (
                    <NIcon>
                      <SearchOutlined />
                    </NIcon>
                  )
                }}
              </NInput>
            </div>
          </NSpace>
          <div class={styles['table-list']}>
            <NDataTable
              loading={loadingRef}
              columns={this.columns}
              data={this.tableData}
              scrollX={this.tableWidth}
            />
          </div>
          <div class={styles.pagination}>
            <NSpace justify='space-between'>
              <span>
                {t('pagination.total')} {this.totalElements}{' '}
                {t('pagination.record')}
              </span>
              <NPagination
                v-model:page={this.page}
                v-model:page-size={this.pageSize}
                pageCount={this.totalPage}
                showSizePicker
                pageSizes={[10, 30, 50]}
                showQuickJumper
                onUpdatePage={changePage}
                onUpdatePageSize={changePageSize}
              />
            </NSpace>
          </div>
        </div>
        <CreateModal
          showModalRef={showModalRef}
          onCancelModal={onCancelModal}
          onConfirmModal={onConfirmModal}
        ></CreateModal>
      </div>
    )
  }
})

export default list
