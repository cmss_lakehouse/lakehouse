/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, PropType } from 'vue'
import { NRadio, NSpace } from 'naive-ui'
import { useForm } from './use-form'
import styles from '../index.module.scss'

const props = {
  value: {
    type: String as PropType<string>,
    default: ''
  },
  updateEdit: {
    type: Function as PropType<(v: any) => void>,
    default: () => {}
  }
}

const Step1 = defineComponent({
  name: 'Step1',
  props,
  setup() {
    const { JOB_TYPE_LIST } = useForm()
    return {
      JOB_TYPE_LIST
    }
  },
  render() {
    const { JOB_TYPE_LIST } = this
    return (
      <NSpace class={styles.step1}>
        {JOB_TYPE_LIST.map((item: any) => {
          return (
            <div class={styles['job-radio']}>
              <NRadio value={item.value}>
                <div class={styles['radio-label']}>{item.label}</div>
                <div class={styles['radio-desc']}>{item.desc}</div>
              </NRadio>
            </div>
          )
        })}
      </NSpace>
    )
  }
})

export default Step1
