/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import { reactive, ref } from 'vue'
import { useI18n } from 'vue-i18n'

export function useForm() {
  const { t } = useI18n()
  const variables = reactive({
    createForm: {
      jobType: '',
      jobName: '',
      jobDesc: '',
      connectorGroup: '',
      connenctorType: '',
      connentorId: '',
      databaseName: '',
      tablePrefix: '',
      storageType: '',
      path: ''
    },
    connectorIdList: ref(),
    databaseList: ref()
  })
  const JOB_TYPE_LIST = [
    {
      label: t('synchronization.create_form.RDB_ALL_label'),
      value: 'RDB_ALL',
      desc: t('synchronization.create_form.RDB_ALL_desc')
    },
    {
      label: t('synchronization.create_form.MQ_REALTIME_label'),
      value: 'MQ_REALTIME',
      desc: t('synchronization.create_form.MQ_REALTIME_desc')
    }
  ]

  const CONNECTOR_GROUP_LIST = [
    {
      label: t('synchronization.create_form.relational_database'),
      value: t('synchronization.create_form.relational_database')
    }
  ]

  const CONNECTOR_TYPE_LIST = [
    {
      label: 'MySQL',
      value: 'MySQL'
    }
  ]
  return {
    t,
    variables,
    JOB_TYPE_LIST,
    CONNECTOR_GROUP_LIST,
    CONNECTOR_TYPE_LIST
  }
}
