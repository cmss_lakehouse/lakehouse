/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, PropType } from 'vue'
import {
  NForm,
  NFormItem,
  NInput,
  NSpace,
  NRadioGroup,
  NRadioButton,
  NDatePicker
} from 'naive-ui'
import { useForm } from './use-form'

const props = {
  value: {
    type: String as PropType<string>,
    default: ''
  },
  updateEdit: {
    type: Function as PropType<(v: any) => void>,
    default: () => {}
  }
}

const Step4 = defineComponent({
  name: 'Step4',
  props,
  setup() {
    const { t } = useForm()
    return {
      t
    }
  },
  render() {
    const { t } = this
    return (
      <NSpace>
        <NForm labelPlacement='left' labelWidth='auto'>
          <NFormItem label={t('synchronization.create_form.schedule_model')}>
            <NRadioGroup>
              <NRadioButton
                label={t('synchronization.create_form.once')}
              ></NRadioButton>
              <NRadioButton
                label={t('synchronization.create_form.cycle')}
              ></NRadioButton>
            </NRadioGroup>
          </NFormItem>
          <NFormItem label={t('synchronization.create_form.schedule_time')}>
            <NDatePicker type='datetime' clearable></NDatePicker>
          </NFormItem>
          <NFormItem label={t('synchronization.create_form.duration_value')}>
            <NDatePicker type='datetime' clearable></NDatePicker>
          </NFormItem>
          <NFormItem
            label={t('synchronization.create_form.schedule_start_time')}
          >
            <NDatePicker type='datetime' clearable></NDatePicker>
          </NFormItem>
          <NFormItem label={t('synchronization.create_form.schedule_end_time')}>
            <NDatePicker type='datetime' clearable></NDatePicker>
          </NFormItem>
          <NFormItem label={t('synchronization.create_form.fail_action')}>
            <NInput></NInput>
          </NFormItem>
        </NForm>
      </NSpace>
    )
  }
})

export default Step4
