/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, PropType } from 'vue'
import { NForm, NFormItem, NSpace, NCard } from 'naive-ui'
import { useForm } from './use-form'

const props = {
  value: {
    type: String as PropType<string>,
    default: ''
  },
  updateEdit: {
    type: Function as PropType<(v: any) => void>,
    default: () => {}
  }
}

const Step5 = defineComponent({
  name: 'Step5',
  props,
  setup() {
    const { t } = useForm()
    return {
      t
    }
  },
  render() {
    const { t } = this
    return (
      <NSpace vertical>
        <NCard
          title={t('synchronization.create_form.step1_title')}
          size='small'
          style='width: 100vh'
        >
          <NForm labelPlacement='left' labelWidth='auto' style='width: 100%'>
            <NFormItem label={t('synchronization.create_form.type')}>
              <span>1</span>
            </NFormItem>
          </NForm>
        </NCard>
        <NCard
          title={t('synchronization.create_form.step2_title')}
          size='small'
          style='width: 100vh'
        >
          <NForm labelPlacement='left' labelWidth='auto'>
            <NFormItem label={t('synchronization.job_name')}>
              <span>1</span>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.job_desc')}>
              <span>1</span>
            </NFormItem>
          </NForm>
        </NCard>
        <NCard
          title={t('synchronization.create_form.data_source_config')}
          size='small'
          style='width: 100vh'
        >
          <NForm labelPlacement='left' labelWidth='auto'>
            <NSpace>
              <NFormItem label={t('synchronization.job_name')}>
                <span>1</span>
              </NFormItem>
              <NFormItem label={t('synchronization.create_form.job_desc')}>
                <span>1</span>
              </NFormItem>
            </NSpace>
          </NForm>
        </NCard>
        <NCard
          title={t('synchronization.create_form.data_target_config')}
          size='small'
          style='width: 100vh'
        >
          <NForm labelPlacement='left' labelWidth='auto'>
            <NSpace>
              <NFormItem label={t('synchronization.job_name')}>
                <span>1</span>
              </NFormItem>
              <NFormItem label={t('synchronization.create_form.job_desc')}>
                <span>1</span>
              </NFormItem>
            </NSpace>
          </NForm>
        </NCard>
        <NCard
          title={t('synchronization.create_form.step4_title')}
          size='small'
          style='width: 100vh'
        >
          <NForm labelPlacement='left' labelWidth='auto'>
            <NFormItem label={t('synchronization.job_name')}>
              <span>1</span>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.job_desc')}>
              <span>1</span>
            </NFormItem>
          </NForm>
        </NCard>
      </NSpace>
    )
  }
})

export default Step5
