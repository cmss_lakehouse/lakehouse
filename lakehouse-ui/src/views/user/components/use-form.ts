/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { useI18n } from 'vue-i18n'
import { reactive, ref } from 'vue'
import { FormRules } from 'naive-ui'

export function useForm() {
  const { t } = useI18n()

  const resetForm = () => {
    variables.formItem = {
      userName: '',
      userPassword: '',
      userPasswordConfirm: '',
      description: ''
    }
  }

  const variables = reactive({
    addUserFormRef: ref(),
    resetPasswordFormRef: ref(),
    formItem: {
      userName: '',
      userPassword: '',
      userPasswordConfirm: '',
      description: ''
    },
    resetPasswordFormItem: {
      userPassword: '',
      userPasswordConfirm: ''
    },
    saving: false,
    rules: {
      userName: {
        trigger: ['input', 'blur'],
        validator() {
          if (variables.formItem.userName === '') {
            return new Error(t('user.addModal.username_tips'))
          }
        }
      },
      userPassword: {
        trigger: ['input', 'blur'],
        validator() {
          if (variables.formItem.userPassword === '') {
            return new Error(t('user.addModal.password_tips'))
          }
        }
      },
      userPasswordConfirm: {
        trigger: ['input', 'blur'],
        validator() {
          if (
            variables.formItem.userPassword !==
            variables.formItem.userPasswordConfirm
          ) {
            return new Error(
              t('user.addModal.two_password_entries_are_inconsistent')
            )
          }
        }
      }
    } as FormRules,
    resetRules: {
      userPassword: {
        trigger: ['input', 'blur'],
        validator() {
          if (variables.resetPasswordFormItem.userPassword === '') {
            return new Error(t('user.resetModal.password_tips'))
          }
        }
      },
      userPasswordConfirm: {
        trigger: ['input', 'blur'],
        validator() {
          if (
            variables.resetPasswordFormItem.userPassword !==
            variables.resetPasswordFormItem.userPasswordConfirm
          ) {
            return new Error(
              t('user.resetModal.two_password_entries_are_inconsistent')
            )
          }
        }
      }
    } as FormRules
  })

  const handleValidate = (formName: string, statusRef: number) => {
    if (formName === 'resetForm') {
      variables.resetPasswordFormRef.validate((errors: any) => {
        if (!errors) {
          statusRef === 0 ? submitProjectModal() : updateProjectModal()
        } else {
          return
        }
      })
    } else {
      variables.addUserFormRef.validate((errors: any) => {
        if (!errors) {
          statusRef === 0 ? submitProjectModal() : updateProjectModal()
        } else {
          return
        }
      })
    }
  }

  const submitProjectModal = async () => {
    if (variables.saving) return
    variables.saving = true
  }

  const updateProjectModal = async () => {
    if (variables.saving) return
    variables.saving = true
  }

  return { variables, t, handleValidate, resetForm }
}
