/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

const props = {
  treeLen: {
    type: Number,
    default: 0
  },
  dbIdsLen: {
    type: Number,
    default: 0
  }
}

import { defineComponent, h } from 'vue'
import { useI18n } from 'vue-i18n'
import styles from '../index.module.scss'

const DefaultPage = defineComponent({
  name: 'default-page',
  props,
  setup(props) {
    const { t } = useI18n()

    const showTip = () => {
      return h(
        'span',
        {},
        {
          default: () => {
            if (props.treeLen === 0) {
              return t('explore.db_not_created')
            }
            if (props.dbIdsLen === 0) {
              return t('explore.db_not_selected')
            }
          }
        }
      )
    }

    return { t, showTip }
  },
  render() {
    return (
      <div class={styles.defaultPage}>
        <div class={styles.bgImg} />
        <div class={styles.text}>{this.showTip()}</div>
      </div>
    )
  }
})

export default DefaultPage
