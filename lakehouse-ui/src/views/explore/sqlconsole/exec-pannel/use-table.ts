/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { onMounted, reactive, watch } from 'vue'
import { useI18n } from 'vue-i18n'

export function useTable() {
  const { t } = useI18n()

  const variables = reactive({
    columns: []
  })

  const createColumns = (variables: any) => {
    variables.columns = [
      {
        title: t('explore.exec_history_taskId'),
        width: 240,
        key: 'taskId'
      },
      {
        title: t('explore.exec_history_submitTime'),
        width: 160,
        key: 'submitTime'
      },
      {
        title: t('instance.engine_type'),
        key: 'engineType',
        width: 90
      },
      {
        title: t('instance.instance_name'),
        key: 'instance',
        width: 140
      },
      {
        title: t('explore.exec_history_sqlContent'),
        key: 'sqlContent',
        width: 220
      },
      {
        title: t('explore.exec_history_status'),
        key: 'status',
        width: 100
      },
      {
        title: t('explore.exec_history_runTime'),
        key: 'runTime',
        width: 130
      }
    ]
  }

  onMounted(() => {
    createColumns(variables)
  })

  watch(useI18n().locale, () => {
    createColumns(variables)
  })

  return {
    variables
  }
}
