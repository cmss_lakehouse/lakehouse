/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { getExecJobHistoryList } from '@/service/modules/sqlconsole'
import {
  HistoryExecJob,
  HistoryExecResult
} from '@/service/modules/sqlconsole/types'
import { Search12Regular } from '@vicons/fluent'
import { useDebounce } from '@vueuse/core'
import { NDataTable, NIcon, NInput } from 'naive-ui'
import { computed, defineComponent, h, reactive, ref, toRefs, watch } from 'vue'
import { useI18n } from 'vue-i18n'
import { useRoute } from 'vue-router'
import useStatus from '../components/use-status'
import styles from './index.module.scss'
import { useTable } from './use-table'

const props = {
  dbName: {
    type: String,
    default: ''
  },
  execDate: {
    type: String,
    default: ''
  },
  bottomDivHeight: {
    type: Number
  }
}

const ExecHistory = defineComponent({
  name: 'exec-history',
  props,
  setup(props) {
    const { t } = useI18n()
    const $route = useRoute()
    const tableLoading = ref<boolean>(false)
    const searchString = ref<string>('')
    const debounced = useDebounce(searchString, 500)
    const data = ref<HistoryExecJob[]>([])
    const { switchStatus } = useStatus()
    const consoleName = computed(() => $route.params.dbId)
    const pageInfo = reactive({
      page: 1,
      pageSize: 10,
      showSizePicker: true,
      pageSizes: [10, 20, 50, 100],
      pageCount: 0
    })
    const variables = useTable()

    const search = () => {
      tableLoading.value = true
      getExecJobHistoryList(
        props.dbName,
        searchString.value,
        pageInfo.page,
        pageInfo.pageSize,
        'DML,DDL,JDBC'
      )
        .then((res: HistoryExecResult) => {
          tableLoading.value = false
          data.value = res.totalList
          pageInfo.pageCount = Math.ceil(res.total / pageInfo.pageSize)
        })
        .catch(() => {
          tableLoading.value = false
        })
    }

    const onPageChange = (page: number) => {
      pageInfo.page = page || 1
      search()
    }

    const onPageSizeChange = (pageSize: number) => {
      pageInfo.pageSize = pageSize || 10
      search()
    }

    watch(
      () => [props.execDate, consoleName.value, debounced.value],
      () => {
        pageInfo.page = 1
        pageInfo.pageSize = 10
        search()
      },
      {
        immediate: true
      }
    )

    return {
      t,
      ...toRefs(variables),
      searchString,
      data,
      tableLoading,
      pageInfo,
      onPageChange,
      onPageSizeChange,
      switchStatus
    }
  },
  render() {
    return (
      <div class={styles.execHistory}>
        <div class={styles.header}>
          <div class={styles.tip}>
            {this.t('explore.exec_history_list_title')}
          </div>
          <NInput
            type='text'
            v-model={[this.searchString, 'value']}
            style='width: 200px;'
            placeholder={this.t('explore.exec_history_search')}
            clearable
          >
            {{
              prefix: () =>
                h(NIcon, null, { default: () => h(Search12Regular) })
            }}
          </NInput>
        </div>
        <div>
          <NDataTable
            remote
            columns={this.variables.columns}
            data={this.data}
            loading={this.tableLoading}
            pagination={this.pageInfo}
            onUpdatePage={this.onPageChange}
            onUpdatePageSize={this.onPageSizeChange}
          />
        </div>
      </div>
    )
  }
})

export default ExecHistory
