/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { useSqlExecTabStore } from '@/store/sqlconsole/sql-exec-tab'
import { SqlExec } from '@/store/sqlconsole/types'
import { defineComponent, inject, ref, watch } from 'vue'
import { SqlConsoleSymbol } from '..'
import useStatus from '../components/use-status'
import ExecHistory from '../exec-pannel/exec-history'
import ExecResult from '../exec-pannel/exec-result'
import TabPane from '../query-panel-option/tab-pane'
import TabsOption from '../query-panel-option/tabs-option'
import styles from './index.module.scss'

const props = {
  dbName: {
    type: String,
    default: ''
  },
  consoleName: {
    type: String,
    default: ''
  },
  bottomDivHeight: {
    type: Number
  },
  execDate: {
    type: String,
    default: ''
  },
  currentExecTab: {
    type: String,
    default: 0
  }
}

const ExecPanel = defineComponent({
  name: 'exec-panel',
  props,
  setup(props) {
    const timerSet = inject(SqlConsoleSymbol, '')
    const execTabs = ref<SqlExec[]>([])
    const activeExecTab = ref<string>('')
    const tabs = ref<number>(1)
    const { switchIcon } = useStatus()
    const $sqlExecTabStore = useSqlExecTabStore()

    watch(
      [() => props.execDate, () => $sqlExecTabStore.getAllExecTabs],
      () => {
        const execStatusTabs = $sqlExecTabStore.getAllExecTabs
        execTabs.value = execStatusTabs.filter(
          (item) =>
            item.consoleName == 'history' ||
            item.consoleName === props.consoleName
        )
      },
      {
        immediate: true
      }
    )

    // close job tab
    const onRemoveJobTab = (jobId: string) => {
      // delete job info in the store
      $sqlExecTabStore.deleteTab(jobId)
      // close timer too
      timerSet.clearTimer(jobId)
    }

    return {
      execTabs,
      activeExecTab,
      tabs,
      switchIcon,
      onRemoveJobTab
    }
  },
  render() {
    return (
      <div class={styles.execPannel}>
        <TabsOption
          tabs={this.execTabs}
          active={this.currentExecTab}
          onTabRemove={this.onRemoveJobTab}
        >
          {{
            default: (activeKey: any) => {
              return this.execTabs.map((tab: SqlExec, index: number) => {
                return (
                  <TabPane key={tab.jobId} class={styles['tab-pane']}>
                    {{
                      default: () =>
                        activeKey === tab.jobId ? (
                          index === 0 ? (
                            <ExecHistory
                              dbName={this.dbName}
                              execDate={this.execDate}
                              bottomDivHeight={this.bottomDivHeight}
                            />
                          ) : (
                            <ExecResult
                              tab={tab}
                              key={tab.jobId}
                              bottomDivHeight={this.bottomDivHeight}
                            />
                          )
                        ) : null
                    }}
                  </TabPane>
                )
              })
            }
          }}
        </TabsOption>
      </div>
    )
  }
})

export default ExecPanel
