/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { useI18n } from 'vue-i18n'
import type { FormRules } from 'naive-ui'
import { reactive, ref } from 'vue'

export function useForm() {
  const { t, locale } = useI18n()

  const variables = reactive({
    loginFormRef: ref(),
    loginForm: {
      userName: '',
      userPassword: ''
    },
    rules: {
      userName: {
        trigger: ['input', 'blur'],
        validator() {
          if (variables.loginForm.userName === '') {
            return new Error(t('login.username_tips'))
          }
        }
      },
      userPassword: {
        trigger: ['input', 'blur'],
        validator() {
          if (variables.loginForm.userPassword === '') {
            return new Error(t('login.password_tips'))
          }
        }
      }
    } as FormRules
  })

  return {
    variables,
    t,
    locale
  }
}
