/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { useRouter } from 'vue-router'
import { login, getCurrentUserInfo } from '@/service/modules/user'
import type { Router } from 'vue-router'
import { useUserStore } from '@/store/user/user'
import { UserInfoDtos } from '@/service/modules/user/types'

export function useLogin(variables: any) {
  const router: Router = useRouter()
  const $userStore = useUserStore()

  const handleLogin = () => {
    variables.loginFormRef.validate(async (valid: any) => {
      if (!valid) {
        const loginRes: any = await login(
          variables.loginForm.userName,
          variables.loginForm.userPassword
        )
        await $userStore.setSessionId(loginRes.sessionId)
        const userInfoRes: UserInfoDtos = await getCurrentUserInfo()
        await $userStore.setUserInfo(userInfoRes)
        router.push({ path: 'home' })
      }
    })
  }

  return {
    handleLogin
  }
}
