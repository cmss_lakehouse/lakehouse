/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, onMounted, watch, toRefs, ref } from 'vue'
import { NLayout, NLayoutSider, NMenu, NSpace, useMessage } from 'naive-ui'
import { useDataList } from './use-dataList'
import { useMenuStore } from '@/store/menu/menu'
import { useLocalesStore } from '@/store/locales/locales'
import { useI18n } from 'vue-i18n'
import { useMenuClick } from './components/sidebar/use-menuClick'
import Navbar from './components/navbar/index'
import { useTheme } from '@/utils/use-theme'
import Logo from './components/logo'

const Content = defineComponent({
  name: 'Content',
  emits: ['handleMenuClick'],
  setup(props, ctx) {
    window.$message = useMessage()

    const menuStore = useMenuStore()
    const { locale } = useI18n()
    const localesStore = useLocalesStore()
    const { state, changeMenuOption, changeUserDropdown } = useDataList()
    const { handleMenuClick } = useMenuClick(ctx)
    const collapsedRef = ref(false)
    const { extendTheme } = useTheme()

    onMounted(() => {
      locale.value = localesStore.getLocales
      changeMenuOption(state)
      changeUserDropdown(state)
    })

    watch(useI18n().locale, () => {
      changeMenuOption(state)
      changeUserDropdown(state)
    })

    return {
      ...toRefs(state),
      menuStore,
      collapsedRef,
      changeMenuOption,
      handleMenuClick,
      changeUserDropdown,
      extendTheme
    }
  },
  render() {
    return (
      <NSpace vertical>
        <NLayout has-sider>
          <NLayoutSider
            bordered
            collapse-mode='width'
            collapsed-width={64}
            width={240}
            collapsed={this.collapsedRef}
            onCollapse={() => (this.collapsedRef = true)}
            onExpand={() => (this.collapsedRef = false)}
          >
            <Logo />
            <NMenu
              value='activeKey'
              collapsed={this.collapsedRef}
              collapsed-width={64}
              collapsed-icon-size={22}
              options={this.menuOptions}
              onUpdateValue={this.handleMenuClick}
            />
          </NLayoutSider>
          <NLayout style={{ 'background-color': '#eff1f4', height: '100vh' }}>
            <Navbar
              localesOptions={this.localesOptions}
              userDropdownOptions={this.userDropdownOptions}
            ></Navbar>
            <router-view key={this.$route.fullPath} />
          </NLayout>
        </NLayout>
      </NSpace>
    )
  }
})

export default Content
