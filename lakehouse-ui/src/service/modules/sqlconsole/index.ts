/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { axios, METADATA_API, SQL_CONSOLE_API } from '@/service/service'
import { Base64 } from 'js-base64'
import {
  HistoryExecResult,
  JobExecResult,
  SqlAbortResult,
  SqlExecResult
} from './types'

/**
 * get metadata tree
 * @returns MetadataTree[]
 */
export function getMetadataTree(): any {
  const url = METADATA_API + 'tree'
  return axios({
    url: url,
    method: 'get'
  })
}

/**
 * get database tree
 * @returns MetadataTree[]
 */
export function getMetadataDatabaseTree(): any {
  const url = METADATA_API + 'database/tree'
  return axios({
    url: url,
    method: 'get'
  })
}

/**
 * get database table tree
 * @param databaseName databaseName
 * @returns MetadataTree[]
 */
export function getMetadataTableTree(databaseName: string): any {
  const url = METADATA_API + `database/table?databaseName=${databaseName}`
  return axios({
    url: url,
    method: 'get'
  })
}

/**
 * get database table column tree
 * @param databaseName databaseName
 * @param tableName tableName
 * @returns MetadataTree[]
 */
export function getMetadataColumnTree(
  databaseName: string,
  tableName: string
): any {
  const url =
    METADATA_API +
    `database/table/column?databaseName=${databaseName}&tableName=${tableName}`
  return axios({
    url: url,
    method: 'get'
  })
}

/**
 * format sql
 * @param instance engine instance
 * @param querySql sql
 * @returns string
 */
export function formatSql(instance: string, querySql: string): Promise<string> {
  const url = SQL_CONSOLE_API + 'format'
  const body = {
    instance: instance,
    querySql: Base64.encode(querySql)
  }
  return axios.post<never, string>(url, body)
}

/**
 * execute sql
 * @param dbName db name
 * @param engineType engine type, spark/presto/flink etd.
 * @param instance instance name
 * @param querySql query sql
 * @returns SqlExecResult
 */
export function executeSql(
  dbName: string,
  engineType: string,
  instance: string,
  querySql: string
): Promise<SqlExecResult> {
  const url = SQL_CONSOLE_API + 'execute'
  const body = {
    dbName: dbName,
    engineType: engineType,
    instance: instance,
    querySql: Base64.encode(querySql.replace(/<br\/>/g, ' '))
  }
  return axios.post<never, SqlExecResult>(url, body)
}

/**
 * execute sql
 * @param dbName db name
 * @param engineType engine type, spark/presto/flink etd.
 * @param instance instance name
 * @param querySql query sql
 * @returns SqlAbortResult
 */
export function abortJob(jobId: string): Promise<SqlAbortResult> {
  const url = SQL_CONSOLE_API + 'abort'
  const body = {
    sqlQueryJobItemDtos: [
      {
        jobId: jobId
      }
    ]
  }
  return axios.get<never, SqlAbortResult>(url, {
    data: body
  })
}

/**
 * query job exec result by jobId
 * @param jobId job id
 * @param engineType engine type, spark/presto/flink etd.
 * @returns SqlAbortResult
 */
export function queryJob(jobId: string, engineType: string) {
  const url = SQL_CONSOLE_API + `${jobId}/query?engineType=${engineType}`
  return axios.post<never, JobExecResult>(url)
}

/**
 * get sql job execution history
 * @param dbName
 * @param searchString
 * @param page
 * @param pageSize
 * @param taskType
 * @returns
 */
export function getExecJobHistoryList(
  dbName: string,
  searchString: string,
  page: number,
  pageSize: number,
  taskType: string
) {
  const url = SQL_CONSOLE_API + 'monitor/list'
  const body = {
    dbName: dbName,
    taskId: searchString || undefined,
    pageDto: {
      limit: pageSize,
      offset: page
    },
    taskType: taskType
  }
  const config = {
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    transformRequest: (params: any) => JSON.stringify(params)
  }
  return axios.post<never, HistoryExecResult>(url, body, config)
}
