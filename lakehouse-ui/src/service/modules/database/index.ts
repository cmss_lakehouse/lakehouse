/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { axios, METADATA_API } from '@/service/service'

/**
 * deleteDatabaseSingleReq
 * @param dbId
 * @returns
 */
export function deleteDatabaseSingleReq(dbId: string): Promise<boolean> {
  const url = METADATA_API + `database/${dbId}`
  return axios.delete(url)
}

/**
 * deleteDatabaseBatchReq
 * @param body
 * @returns
 */
export function deleteDatabaseBatchReq(body: any): Promise<any> {
  const url = METADATA_API + 'database/batch-delete'
  return axios.post(url, body)
}

/**
 * get database list
 * @param query
 * @returns
 */
export function getDatabaseList(query: any): Promise<any> {
  const pageNum = query.pageNum ? `?pageNum=${query.pageNum}` : ''
  const pageSize = query.pageSize ? `&pageSize=${query.pageSize}` : ''
  const name = query.name ? `&name=${query.name}` : ''
  const url = METADATA_API + 'database' + pageNum + pageSize + name
  return axios.get(url)
}

/**
 * create database
 * @param body
 * @returns
 */
export function createDatabaseReq(body: any): Promise<any> {
  const url = '/database'
  return axios.post(url, body)
}

/**
 * edit database
 * @param dbId
 * @param body
 * @returns
 */
export function editDatabaseReq(dbId: any, body: any): Promise<any> {
  const url = `database/${dbId}`
  return axios.put(url, body)
}

/**
 * get database count
 * @returns
 */
export function getDatabaseCount(): Promise<any> {
  const url = '/database/user/count'
  return axios.get(url)
}

/**
 * check database name is duplicate name
 * @param dbName
 * @returns
 */
export function isDuplicateName(dbName: string): Promise<any> {
  const url = `database/${dbName}/exists`
  return axios.get(url)
}
