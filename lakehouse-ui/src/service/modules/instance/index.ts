/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { axios, INSTANCE_API } from '@/service/service'
import { CreateInfo } from './types'

/**
 * Create Instance
 * @param body
 * @returns
 */
export function createInstance(body: CreateInfo): Promise<string> {
  const url = INSTANCE_API + 'create'
  const headers = {
    'Content-Type': 'application/json'
  }
  return axios.post<never, string>(url, JSON.stringify(body), {
    headers: headers
  })
}

export function getInstanceList(engineType: string): Promise<string[]> {
  const url = INSTANCE_API + 'list?engineType=' + engineType
  return axios.get<never, string[]>(url)
}

export function getInstanceListPaging(body: any): Promise<any> {
  const url =
    INSTANCE_API +
    `list-paging?pageNo=${body.pageNum}&pageSize=${body.pageSize}&searchVal=${body.searchVal}`
  return axios.get(url)
}

export function releaseInstance(instanceId: string): Promise<any> {
  const url = INSTANCE_API + `${instanceId}/release`
  return axios.post(url)
}

export function getInstanceDetailsById(instanceId: string): Promise<any> {
  const url = INSTANCE_API + `${instanceId}`
  return axios.get(url)
}
