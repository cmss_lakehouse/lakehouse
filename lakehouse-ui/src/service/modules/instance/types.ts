/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

export interface InsatnceReq {
  clusterName: string
  clusterType: string
  computeResourceSize: string
  createdBy: string
}

export interface CreateInfo {
  instance: string
  clusterType: string
  computeResourceSize: string
  engineType: string
  userName: string
}

export interface Instance {
  instanceId: string
  clusterName: string
  region: string
  billingType: string
  computeResourceSize: string
  status: string
  runtime: string
  createTime: string
  pool: string
}
export interface InstanceList {
  offset: number
  limit: number
  totalPage: number
  resultList: Instance[]
}
