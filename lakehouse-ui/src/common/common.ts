/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
import runningIcon from '@/assets/images/instance/running.svg'
import creatingIcon from '@/assets/images/instance/creating.gif'
import failedIcon from '@/assets/images/instance/failed.svg'
import errorIcon from '@/assets/images/instance/error.svg'

export const computeResourceSizeType = (t: any) => ({
  SMALL: {
    desc: `${t('instance.size_types.small')}`
  },
  MEDIUM: {
    desc: `${t('instance.size_types.medium')}`
  },
  LARGE: {
    desc: `${t('instance.size_types.large')}`
  },
  XLARGE: {
    desc: `${t('instance.size_types.xlarge')}`
  }
})

export const instanceStatusType = (t: any) => ({
  running: {
    desc: `${t('instance.status_types.running')}`,
    icon: runningIcon
  },
  creating: {
    desc: `${t('instance.status_types.creating')}`,
    icon: creatingIcon
  },
  create_failed: {
    desc: `${t('instance.status_types.create_failed')}`,
    icon: failedIcon
  },
  error: {
    desc: `${t('instance.status_types.error')}`,
    icon: errorIcon
  },
  accepted: {
    desc: `${t('instance.status_types.accepted')}`,
    icon: creatingIcon
  }
})
