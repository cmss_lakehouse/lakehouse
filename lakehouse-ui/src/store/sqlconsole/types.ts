/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

export interface TabRouteConfig {
  name: string
  path: string
  query: any
  label: string
}

export interface TabRouteState {
  routers: TabRouteConfig[] // router configuration
  dbIds: string[] // tab id list
}

// auto completion struct
// with 3 level: database.table.columns
export interface TableStruct {
  [key: string]: {
    [key: string]: string[]
  }
}

export interface CompletionSqlState {
  data: TableStruct /** exec result */
}

// sql exec struct
export interface SqlExec {
  [x: string]: any
  jobId: string /** jobId */
  label: string /** execute job name */
  sql?: string
  status?: string
  consoleName?: string /** execute console name */
  closable: boolean /** can be closed */
  columnList?: string[] /** column list */
  resultSet?: string[][] /** result list, one-to-one corresponding with column list */
  resultString?: string /** abnormal exec result */
}

export interface SqlExecState {
  tabs: SqlExec[] /** execute job list */
}

// exec job history
export interface HistoryExecJob {
  engineType: string
  finishTime: string
  instance: string
  runTime: number
  sqlContent: string
  status: string
  submitTime: string
  submitUser: string
  taskId: string
  taskType: string
  waitTime: number
}

export type HistoryExecResult = {
  rows: HistoryExecJob[]
  total: number
}
