/* eslint-disable @typescript-eslint/no-unused-vars */
/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineStore } from 'pinia'
import { SqlExec, SqlExecState } from './types'

export const useSqlExecTabStore = defineStore({
  id: 'sqlExecTab',
  state: (): SqlExecState => ({
    tabs: [
      {
        jobId: 'job_history',
        label: 'execution history',
        status: 'null',
        consoleName: 'history',
        closable: false
      }
    ]
  }),
  getters: {
    getAllExecTabs(): SqlExec[] {
      return this.tabs
    },
    getExecTabByJob(): any {
      return (jobId: string) =>
        this.tabs.find((job: SqlExec) => job.jobId === jobId)
    }
  },
  actions: {
    addTab(job: SqlExec) {
      // when sql submitted then return job status list, add job
      this.tabs.push(job)
    },
    updateTab(job: SqlExec) {
      this.tabs.map((tab: SqlExec) => {
        if (job.jobId === tab.jobId) {
          tab.status = job.status
          tab.label = job.label
          tab.columnList = job.columnList
          tab.resultSet = job.resultSet
          tab.resultString = job.resultString
        }
      })
    },
    deleteTab(jobId: string) {
      // close job execution tab
      this.tabs = this.tabs.filter((item: SqlExec) => {
        return item.jobId !== jobId
      })
    },
    deleteTabByConsoleName(consoleName: string) {
      this.tabs = this.tabs.filter((item: SqlExec) => {
        // when close current sql query console tab, then close all execution jobs under this tab.
        return item.consoleName !== consoleName
      })
    },
    clearTab() {
      this.tabs = [
        {
          jobId: 'job_history',
          label: 'execution history',
          status: 'null',
          consoleName: 'history',
          closable: false
        }
      ]
    }
  }
})
