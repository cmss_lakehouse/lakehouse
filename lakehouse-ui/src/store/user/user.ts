/* eslint-disable @typescript-eslint/no-unused-vars */
/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { UserInfoDtos } from '@/service/modules/user/types'
import { defineStore } from 'pinia'
import { UserState } from './types'

export const useUserStore = defineStore({
  id: 'user',
  state: (): UserState => ({
    sessionId: '',
    userInfo: {}
  }),
  persist: true,
  getters: {
    getSessionId(): string {
      return this.sessionId
    },
    getUserInfo(): UserInfoDtos | {} {
      return this.userInfo
    }
  },
  actions: {
    setSessionId(sessionId: string): void {
      this.sessionId = sessionId
    },
    setUserInfo(userInfo: UserInfoDtos | {}): void {
      this.userInfo = userInfo
    }
  }
})
