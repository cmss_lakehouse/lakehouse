/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineStore } from 'pinia'
import MenuState from './types'

export const useMenuStore = defineStore({
  id: 'menu',
  state: (): MenuState => ({
    menuKey: 'home'
  }),
  persist: true,
  getters: {
    getMenuKey(): string {
      return this.menuKey
    }
  },
  actions: {
    setMenuKey(menuKey: string): void {
      this.menuKey = menuKey
    }
  }
})
