/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, onMounted, toRefs, watch } from 'vue'
import styles from './index.module.scss'
import Locales from '@/layouts/content/components/locales'
import User from '@/layouts/content/components/user'
import { useI18n } from 'vue-i18n'
import { useLocalesStore } from '@/store/locales/locales'
import { useDataList } from '@/layouts/content/use-dataList'

const HeadNav = defineComponent({
  name: 'HeadNav',
  setup() {
    const { locale, t } = useI18n()
    const localesStore = useLocalesStore()
    const { state, changeMenuOption, changeUserDropdown } = useDataList()

    onMounted(() => {
      locale.value = localesStore.getLocales
      changeMenuOption(state)
      changeUserDropdown(state)
    })

    watch(useI18n().locale, () => {
      changeMenuOption(state)
      changeUserDropdown(state)
    })

    return {
      t,
      ...toRefs(state)
    }
  },
  render() {
    return (
      <div>
        <div class={styles.container}>
          <div class={styles.logo}>
            <div class={styles['logo-img']} />
            <div class={styles['logo-bg-img']} />
            <div class={styles.title}>
              <div>云原生大数据分析 LakeHouse</div>
            </div>
          </div>
          <div class={styles.settings}>
            <div class={styles.doc}>{this.t('common.help_center')}</div>
            <Locales localesOptions={this.localesOptions} />
            <User userDropdownOptions={this.userDropdownOptions} />
          </div>
        </div>
        <div class={styles.footer}>
          <router-view name='sql-console-container'></router-view>
        </div>
      </div>
    )
  }
})

export default HeadNav
