/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { GlobalThemeOverrides } from 'naive-ui'
import { composite } from 'seemly'

function createHoverColor(color: string, overlayAlpha = 0.15): string {
  return composite(color, [255, 255, 255, overlayAlpha])
}

function createPressedColor(color: string, overlayAlpha = 0.15): string {
  return composite(color, [0, 0, 0, overlayAlpha])
}

export function useTheme() {
  const themeOverrides: GlobalThemeOverrides = {
    common: {
      primaryColor: '#4759E4',
      primaryColorHover: createHoverColor('#4759E4'),
      primaryColorPressed: createPressedColor('#4759E4'),
      successColor: '#10C038',
      successColorHover: createHoverColor('#10C038'),
      successColorPressed: createPressedColor('#10C038'),
      errorColor: '#F04134',
      errorColorHover: createHoverColor('#F04134'),
      errorColorPressed: createPressedColor('#F04134'),
      warningColor: '#FF931D',
      warningColorHover: createHoverColor('#FF931D', 0.2),
      warningColorPressed: createPressedColor('#FF931D', 0.05),
      textColorDisabled: '#D7DAE0',
      textColor1: '#333',
      textColor2: '#333'
    }
  }

  const extendTheme = {
    navBgColor: '#353D65',
    navBgColorHover: '#303860',
    navSideBorderColor: '#6F80FF',
    borderBottomSlide: '#4759E4'
  }

  return {
    themeOverrides,
    extendTheme
  }
}
