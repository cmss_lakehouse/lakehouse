/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import type { Component } from 'vue'

const mapping = (modules: any) => {
  const components: { [key: string]: Component } = {}
  // All TSX files under the views folder automatically generate mapping relationship
  Object.keys(modules).forEach((key: string) => {
    const nameMatch: string[] | null = key.match(/^\/src\/views\/(.+)\.tsx/)

    if (!nameMatch) {
      return
    }

    // If the page is named Index, the parent folder is used as the name
    const indexMatch: string[] | null = nameMatch[1].match(/(.*)\/Index$/i)

    let name: string = indexMatch ? indexMatch[1] : nameMatch[1]

    name = name.replaceAll('/', '-')

    components[name] = modules[key]
  })
  return components
}

export default mapping
