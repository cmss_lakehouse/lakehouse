/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import type { RouteRecordRaw } from 'vue-router'
import type { Component } from 'vue'
import utils from '@/utils'
import exploreRouter from './modules/explore'
import instanceRouter from './modules/instance'
import synchronizationRouter from './modules/synchronization'
import discoveryRouter from './modules/discovery'

// All TSX files under the views folder automatically generate mapping relationship
const modules = import.meta.glob('/src/views/**/**.tsx')
const components: { [key: string]: Component } = utils.mapping(modules)

/**
 * Basic page
 */
const basePage: RouteRecordRaw[] = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    meta: { title: '登录' }
  },
  {
    path: '/',
    redirect: { name: 'home' },
    meta: { title: '概览' },
    component: () => import('@/layouts/content'),
    children: [
      {
        path: '/home',
        name: 'home',
        component: components['home'],
        meta: {
          title: '概览',
          auth: []
        }
      },
      {
        path: '/database',
        name: 'databse',
        component: components['database'],
        meta: {
          title: '数据库',
          auth: []
        }
      },
      {
        path: '/directory',
        name: 'directory',
        component: components['directory'],
        meta: {
          title: '数据目录',
          auth: []
        }
      },
      {
        path: '/user',
        name: 'user',
        component: components['user'],
        meta: {
          title: '用户管理',
          auth: []
        }
      }
    ]
  },
  {
    path: '/details',
    redirect: ''
  },
  exploreRouter,
  instanceRouter,
  synchronizationRouter,
  discoveryRouter
]

const routes: RouteRecordRaw[] = [...basePage]

export default routes
