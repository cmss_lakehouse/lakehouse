/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, ref, nextTick, provide, watch } from 'vue'
import {
  zhCN,
  enUS,
  dateZhCN,
  dateEnUS,
  NConfigProvider,
  NMessageProvider,
  NDialogProvider
} from 'naive-ui'
import { useLocalesStore } from '@/store/locales/locales'
import { useRoute } from 'vue-router'
import { useTabsRouteStore } from './store/sqlconsole/tabs-route'
import { useTheme } from './utils/use-theme'

const App = defineComponent({
  name: 'App',
  setup() {
    const isRouterAlive = ref(true)
    const localesStore = useLocalesStore()
    const $route = useRoute()
    const $tabsRouteStore = useTabsRouteStore()
    const { themeOverrides } = useTheme()
    /*refresh page when router params change*/
    const reload = () => {
      isRouterAlive.value = false
      nextTick(() => {
        isRouterAlive.value = true
      })
    }

    provide('reload', reload)

    watch(
      () => $route.fullPath,
      () => {
        if ($route.name === 'explore-sqlconsole-tab') {
          $tabsRouteStore.addTabRoute($route)
        }
      }
    )

    return {
      reload,
      isRouterAlive,
      localesStore,
      themeOverrides
    }
  },
  render() {
    return (
      <NConfigProvider
        style={{ width: '100%', height: '100vh' }}
        theme-overrides={this.themeOverrides}
        date-locale={
          String(this.localesStore.getLocales) === 'zh_CN' ? dateZhCN : dateEnUS
        }
        locale={String(this.localesStore.getLocales) === 'zh_CN' ? zhCN : enUS}
      >
        <NMessageProvider>
          <NDialogProvider>
            {this.isRouterAlive ? <router-view /> : ''}
          </NDialogProvider>
        </NMessageProvider>
      </NConfigProvider>
    )
  }
})

export default App
