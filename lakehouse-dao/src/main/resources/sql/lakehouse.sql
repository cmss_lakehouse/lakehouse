/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

SET FOREIGN_KEY_CHECKS=0;

-- UUID_TO_BIN FUNC
DELIMITER $$
CREATE FUNCTION UUID_TO_BIN(uuid CHAR(36), f BOOLEAN)
RETURNS BINARY(16)
DETERMINISTIC
BEGIN
  RETURN LOWER(UNHEX(CONCAT(
  IF(f,SUBSTRING(uuid, 15, 4),SUBSTRING(uuid, 1, 8)),
  SUBSTRING(uuid, 10, 4),
  IF(f,SUBSTRING(uuid, 1, 8),SUBSTRING(uuid, 15, 4)),
  SUBSTRING(uuid, 20, 4),
  SUBSTRING(uuid, 25)))
  );
END$$
DELIMITER ;

DROP TABLE IF EXISTS `cluster_info`;
CREATE TABLE IF NOT EXISTS `cluster_info` (
    `instance_id`           VARCHAR(64)  PRIMARY KEY NOT NULL,
    `instance`              VARCHAR(128) NOT NULL UNIQUE,
    `user_id`               VARCHAR(64) NOT NULL,
    `cluster_type`          VARCHAR(32)  NOT NULL,
    `create_user`           VARCHAR(128) NOT NULL,
    `compute_resource_size` VARCHAR(32),
    `storage_usage`         TEXT,
    `create_time`           DATETIME     NOT NULL,
    `update_time`           DATETIME,
    `release_time`          DATETIME,
    `create_retries`        BIGINT,
    `status`                VARCHAR(128) NOT NULL,
    `task_parallelize`      TINYINT(1) DEFAULT 3,
    `version`               VARCHAR(32),
    `deleted`               TINYINT(1) DEFAULT 0,
    `expire_time`           DATETIME,
    `engine_type`           VARCHAR(32)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `engine_task_info`;
CREATE TABLE IF NOT EXISTS `engine_task_info` (
    `id`                           BIGINT PRIMARY KEY AUTO_INCREMENT,
    `name`                         VARCHAR(128),
    `instance`                     VARCHAR(128),
    `db_name`                      VARCHAR(128),
    `task_id`                      VARCHAR(128),
    `driver_pod_id`                VARCHAR(128),
    `create_time`                  DATETIME,
    `submit_time`                  DATETIME,
    `update_time`                  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `task_result_redis_key`        VARCHAR(128),
    `task_query_metastore_url`     VARCHAR(256),
    `task_query_metastore_catalog` VARCHAR(128),
    `task_query_redis_host`        VARCHAR(128),
    `task_query_redis_port`        VARCHAR(128),
    `task_query_redis_key`         VARCHAR(128),
    `sql_content`                  TEXT,
    `canceled`                     TINYINT(1),
    `status`                       VARCHAR(128),
    `create_user`                  VARCHAR(128),
    `submit_user`                  VARCHAR(128),
    `retry_times`                  BIGINT            default 1,
    `log_path`                     VARCHAR(256),
    `engine_type`                  VARCHAR(128),
    `task_type`                    VARCHAR(128),
    `wait_time`                    BIGINT,
    `run_time`                     BIGINT,
    `result_rows`                  BIGINT,
    `file_path`                    VARCHAR(256),
    `main_class`                   VARCHAR(256),
    `engine_args`                  TEXT,
    `engine_config`                TEXT,
    `version`                      int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `spark_filename_task`;
CREATE TABLE IF NOT EXISTS `spark_filename_task` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
    `filename` varchar(255) DEFAULT NULL COMMENT 'filename',
    `task_id` varchar(255) DEFAULT NULL COMMENT 'task_id',
    `create_user` varchar(255) DEFAULT NULL COMMENT 'create_user',
    `create_user_id` varchar(255) DEFAULT NULL COMMENT 'create_user_id',
    `config_content` text COMMENT 'config_content',
    `file_id` varchar(255) COMMENT 'file_id',
    `schedule_model` varchar(255) COMMENT 'schedule_model',
    `related_task_id` varchar(255) COMMENT 'related_task_id',
    `related_task_name` varchar(255) COMMENT 'related_task_name',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
    `id`                           BIGINT PRIMARY KEY AUTO_INCREMENT,
    `event`                        VARCHAR(128),
    `instance_id`                  VARCHAR(128),
    `time`                         DATETIME
);

DROP TABLE IF EXISTS `user_info`;
CREATE TABLE IF NOT EXISTS `user_info` (
    `user_id`                      VARCHAR(64) NOT NULL,
    `user_name`                    VARCHAR(64) DEFAULT NULL,
    `user_password`                VARCHAR(64) DEFAULT NULL,
    `user_type`                    TINYINT(4) DEFAULT NULL,
    `create_time`                  DATETIME NOT NULL,
    `update_time`                  DATETIME,
    `description`                  VARCHAR(64) DEFAULT NULL,
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Records of user_info
INSERT INTO user_info(user_name, user_password, user_id, user_type, create_time, update_time, description)
VALUES ('admin', 'lakehouse@123', LOWER(HEX(UUID_TO_BIN(UUID(), 0))), 0, '2022-05-20 08:11:20', '2022-05-20 08:11:26', 'default admin user');

DROP TABLE IF EXISTS `user_access_record_info`;
CREATE TABLE IF NOT EXISTS `user_access_record_info` (
    `id`                           BIGINT PRIMARY KEY AUTO_INCREMENT,
    `access`                       varchar(255) DEFAULT NULL,
    `instance`                     varchar(128) DEFAULT NULL,
    `create_at`                    datetime DEFAULT NULL,
    `is_master`                    bit(1) DEFAULT NULL,
    `update_at`                    datetime DEFAULT NULL,
    `user_id`                      varchar(64) NOT NULL,
    `user_name`                    varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_JOB_DETAILS;
CREATE TABLE QRTZ_JOB_DETAILS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    JOB_NAME  VARCHAR(200) NOT NULL,
    JOB_GROUP VARCHAR(200) NOT NULL,
    DESCRIPTION VARCHAR(250) NULL,
    JOB_CLASS_NAME   VARCHAR(250) NOT NULL,
    IS_DURABLE VARCHAR(1) NOT NULL,
    IS_NONCONCURRENT VARCHAR(1) NOT NULL,
    IS_UPDATE_DATA VARCHAR(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR(1) NOT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_TRIGGERS;
CREATE TABLE QRTZ_TRIGGERS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    JOB_NAME  VARCHAR(200) NOT NULL,
    JOB_GROUP VARCHAR(200) NOT NULL,
    DESCRIPTION VARCHAR(250) NULL,
    NEXT_FIRE_TIME BIGINT(13) NULL,
    PREV_FIRE_TIME BIGINT(13) NULL,
    PRIORITY INTEGER NULL,
    TRIGGER_STATE VARCHAR(16) NOT NULL,
    TRIGGER_TYPE VARCHAR(8) NOT NULL,
    START_TIME BIGINT(13) NOT NULL,
    END_TIME BIGINT(13) NULL,
    CALENDAR_NAME VARCHAR(200) NULL,
    MISFIRE_INSTR SMALLINT(2) NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
        REFERENCES QRTZ_JOB_DETAILS(SCHED_NAME,JOB_NAME,JOB_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_SIMPLE_TRIGGERS;
CREATE TABLE QRTZ_SIMPLE_TRIGGERS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    REPEAT_COUNT BIGINT(7) NOT NULL,
    REPEAT_INTERVAL BIGINT(12) NOT NULL,
    TIMES_TRIGGERED BIGINT(10) NOT NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_CRON_TRIGGERS;
CREATE TABLE QRTZ_CRON_TRIGGERS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    CRON_EXPRESSION VARCHAR(200) NOT NULL,
    TIME_ZONE_ID VARCHAR(80),
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_SIMPROP_TRIGGERS;
CREATE TABLE QRTZ_SIMPROP_TRIGGERS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    STR_PROP_1 VARCHAR(512) NULL,
    STR_PROP_2 VARCHAR(512) NULL,
    STR_PROP_3 VARCHAR(512) NULL,
    INT_PROP_1 INT NULL,
    INT_PROP_2 INT NULL,
    LONG_PROP_1 BIGINT NULL,
    LONG_PROP_2 BIGINT NULL,
    DEC_PROP_1 NUMERIC(13,4) NULL,
    DEC_PROP_2 NUMERIC(13,4) NULL,
    BOOL_PROP_1 VARCHAR(1) NULL,
    BOOL_PROP_2 VARCHAR(1) NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
    REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_BLOB_TRIGGERS;
CREATE TABLE QRTZ_BLOB_TRIGGERS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    BLOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_CALENDARS;
CREATE TABLE QRTZ_CALENDARS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    CALENDAR_NAME  VARCHAR(200) NOT NULL,
    CALENDAR BLOB NOT NULL,
    PRIMARY KEY (SCHED_NAME,CALENDAR_NAME)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_PAUSED_TRIGGER_GRPS;
CREATE TABLE QRTZ_PAUSED_TRIGGER_GRPS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_GROUP  VARCHAR(200) NOT NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_FIRED_TRIGGERS;
CREATE TABLE QRTZ_FIRED_TRIGGERS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    ENTRY_ID VARCHAR(95) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    INSTANCE_NAME VARCHAR(200) NOT NULL,
    FIRED_TIME BIGINT(13) NOT NULL,
    SCHED_TIME BIGINT(13) NOT NULL,
    PRIORITY INTEGER NOT NULL,
    STATE VARCHAR(16) NOT NULL,
    JOB_NAME VARCHAR(200) NULL,
    JOB_GROUP VARCHAR(200) NULL,
    IS_NONCONCURRENT VARCHAR(1) NULL,
    REQUESTS_RECOVERY VARCHAR(1) NULL,
    PRIMARY KEY (SCHED_NAME,ENTRY_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_SCHEDULER_STATE;
CREATE TABLE QRTZ_SCHEDULER_STATE
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    INSTANCE_NAME VARCHAR(200) NOT NULL,
    LAST_CHECKIN_TIME BIGINT(13) NOT NULL,
    CHECKIN_INTERVAL BIGINT(13) NOT NULL,
    PRIMARY KEY (SCHED_NAME,INSTANCE_NAME)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS QRTZ_LOCKS;
CREATE TABLE QRTZ_LOCKS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    LOCK_NAME  VARCHAR(40) NOT NULL,
    PRIMARY KEY (SCHED_NAME,LOCK_NAME)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_session`;
CREATE TABLE IF NOT EXISTS `user_session` (
	`id` BIGINT NOT NULL AUTO_INCREMENT,
	`ip` VARCHAR(255) NOT NULL,
	`last_login_time` DATETIME NOT NULL,
	`user_id` VARCHAR(64) NOT NULL,
	`uuid` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_access_token`;
CREATE TABLE IF NOT EXISTS `user_access_token` (
	`id` BIGINT NOT NULL AUTO_INCREMENT,
	`create_time` DATETIME NOT NULL,
	`expire_time` DATETIME NOT NULL,
	`token` VARCHAR(255) NOT NULL,
	`update_time` DATETIME NOT NULL,
	`user_id` VARCHAR(64) NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hive_dbs`;
CREATE TABLE `hive_dbs` (
  `DB_ID` bigint(20) NOT NULL,
  `DESC` varchar(4000) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `DB_LOCATION_URI` varchar(4000) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `NAME` varchar(128) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `OWNER_NAME` varchar(128) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `OWNER_TYPE` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `CTLG_NAME` varchar(256) NOT NULL DEFAULT 'hive',
  `CREATE_TIME` int(11) DEFAULT NULL,
  `DB_MANAGED_LOCATION_URI` varchar(4000) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `TYPE` varchar(32) NOT NULL DEFAULT 'NATIVE',
  `DATACONNECTOR_NAME` varchar(128) DEFAULT NULL,
  `REMOTE_DBNAME` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`DB_ID`),
  UNIQUE KEY `UNIQUE_DATABASE` (`NAME`,`CTLG_NAME`),
  UNIQUE KEY `UNIQUEDATABASE` (`NAME`,`CTLG_NAME`),
  KEY `CTLG_FK1` (`CTLG_NAME`),
  CONSTRAINT `CTLG_FK1` FOREIGN KEY (`CTLG_NAME`) REFERENCES `ctlgs` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `metadata_dbs`;
CREATE TABLE `metadata_dbs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_time` int(11) DEFAULT NULL COMMENT 'create_time',
  `creator` varchar(255) DEFAULT NULL COMMENT 'creator',
  `deleted` bit(1) DEFAULT NULL COMMENT 'delete or not',
  `description` varchar(255) DEFAULT NULL COMMENT 'description',
  `name` varchar(255) DEFAULT NULL COMMENT 'name',
  `update_time` int(11) DEFAULT NULL COMMENT 'update_time',
  `updator` varchar(255) DEFAULT NULL COMMENT 'updator',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `hive_table`;
CREATE TABLE `hive_table` (
  `tbl_id` bigint(20) NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `is_rewrite_enabled` bit(1) DEFAULT NULL,
  `last_access_time` int(11) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `owner_type` varchar(255) DEFAULT NULL,
  `retention` varchar(255) DEFAULT NULL,
  `tbl_name` varchar(255) DEFAULT NULL,
  `tbl_type` varchar(255) DEFAULT NULL,
  `db_id` bigint(20) DEFAULT NULL,
  `sd_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`tbl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hive_table_params`;
CREATE TABLE `hive_table_params` (
  `param_key` varchar(255) NOT NULL,
  `tbl_id` int(11) NOT NULL,
  `param_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`param_key`,`tbl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `table_task_inf`;
CREATE TABLE `table_task_inf` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` int(11) DEFAULT NULL,
  `creator` varchar(256) DEFAULT NULL,
  `db_name` varchar(256) DEFAULT NULL,
  `tb_name` varchar(256) DEFAULT NULL,
  `task_id` varchar(256) DEFAULT NULL,
  `task_type` smallint(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `table_task_ref_DB_NAME_TB_NAME_UNIQUE_IDX` (`db_name`,`tb_name`),
  KEY `table_task_ref_TASK_ID_IDX` (`task_id`),
  KEY `table_task_ref_DB_NAME_IDX` (`db_name`),
  KEY `table_task_ref_TB_NAME_IDX` (`tb_name`),
  KEY `table_task_ref_CREATE_TIME_IDX` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sds`;
CREATE TABLE `sds` (
  `sd_id` bigint(20) NOT NULL,
  `cd_id` bigint(20) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `data_source`;
CREATE TABLE IF NOT EXISTS `data_source` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255)  NOT NULL COMMENT '数据源名称',
  `data_source_type` varchar(255) NULL COMMENT '数据源分类',
  `ip` varchar(512)  DEFAULT NULL COMMENT '数据源ip',
  `port` int(11) unsigned DEFAULT NULL COMMENT '数据源端口',
  `url` varchar(512)  DEFAULT NULL COMMENT '数据源url',
  `username` varchar(255)  DEFAULT NULL COMMENT '用户名',
  `password` varchar(255)  DEFAULT NULL COMMENT '密码(加密形式)',
  `extend_param` varchar(1024)  DEFAULT NULL COMMENT '扩展配置项',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否软删除，0：正常(默认值)，1：软删除',
  `create_user_id` varchar(255)  DEFAULT NULL COMMENT '创建人id',
  `create_time` datetime NULL COMMENT '创建时间',
  `update_user_id` varchar(255)  DEFAULT NULL COMMENT '修改人id',
  `update_time` datetime NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
	`name` VARCHAR(255) NOT NULL COMMENT '任务名称',
	`description` VARCHAR(255) NULL DEFAULT NULL COMMENT '任务描述',
	`job_type` TINYINT(1) NULL DEFAULT NULL COMMENT '迁移任务类型，0离线，1：实时',
	`source_id` BIGINT(20) NULL COMMENT '数据源ID，来自datasource的ID',
	`source_param` mediumtext CHARACTER SET utf8 COMMENT '任务数据源配置',
	`target_id` BIGINT(20) NULL COMMENT '数据目标ID，来自datasource的ID',
	`target_param` mediumtext CHARACTER SET utf8 COMMENT '任务数据目标配置',
	`instance_id` VARCHAR(50) NULL DEFAULT NULL COMMENT '实例ID',
	`instance_name` VARCHAR(50) NULL DEFAULT NULL COMMENT '实例Name',
	`pool_id` VARCHAR(255) NULL DEFAULT NULL COMMENT '存储资源池ID',
	`deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否软删除，0：正常(默认值)，1：软删除',
	`schedule_info_id` BIGINT(20) NULL COMMENT '调度信息ID',
	`running_status` TINYINT(1) NULL DEFAULT NULL COMMENT '任务运行状态，0运行中，1成功，2失败，3已中断，4，5中断中',
	`create_user_id` VARCHAR(255) NULL DEFAULT NULL COMMENT '创建人ID',
	`create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
	`update_user_id` VARCHAR(255) NULL DEFAULT NULL COMMENT '更新人ID',
	`update_time` DATETIME NULL DEFAULT NULL COMMENT '更新时间',
	  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedule_info`;
CREATE TABLE `schedule_info` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键自增id',
	`schedule_model` TINYINT(1) NULL DEFAULT NULL COMMENT '调度方式：0:周期运行，1：单次调度',
	`schedule_status` TINYINT(1) NULL DEFAULT NULL COMMENT '调度状态，1：开启，0：停止(默认)。',
	`duration_unit` TINYINT(1) NULL DEFAULT NULL COMMENT 'MINUTES(4):分钟，HOURS(5):小时，DAYS(7):天，WEEKS(8):周，MONTHS(9):月',
	`duration_value` INT(4) NULL DEFAULT NULL COMMENT '调度周期',
	`fail_action` TINYINT(1) NULL DEFAULT NULL COMMENT '任务失败策略，0: 单次任务失败后，不再执行后续的调度任务,1: 单次任务失败后，忽略失败，并执行下一次调度任务',
	`cron_trigger` VARCHAR(255) NULL DEFAULT NULL COMMENT '由配置的调度时间生成cron表达式' COLLATE 'utf8_general_ci',
	`schedule_start_time` DATETIME NULL DEFAULT NULL COMMENT '调度开始时间',
	`schedule_end_time` DATETIME NULL DEFAULT NULL COMMENT '调度结束时间',
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `job_history`;
CREATE TABLE `job_history` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`job_id` BIGINT(20) NULL DEFAULT NULL COMMENT '任务id',
	`job_type` TINYINT(1) NULL DEFAULT NULL COMMENT '任务类型',
	`status` TINYINT(4) NULL DEFAULT NULL COMMENT '执行状态',
	`progress_rate` TINYINT(4) NULL DEFAULT NULL COMMENT '执行进度',
	`sub_task_num` INT(11) NULL DEFAULT NULL COMMENT '子任务作业数量',
	`write_num` BIGINT(20) NULL DEFAULT NULL COMMENT '任务已经写入的记录数',
	`start_time` DATETIME NULL DEFAULT NULL COMMENT '开始时间',
	`end_time` DATETIME NULL DEFAULT NULL COMMENT '结束时间',
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `job_task_history`;
CREATE TABLE `job_task_history` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`job_history_id` BIGINT(20) NULL DEFAULT NULL COMMENT '对应job_history的id',
	`flink_web_url` VARCHAR(255) NULL DEFAULT NULL COMMENT '对应提交到flink集群的WENUI的url。' ,
	`flinkx_config` MEDIUMTEXT NULL DEFAULT NULL COMMENT '对应任务提交时的配置,其他组件直接提交过来的flinkx的json配置文件',
	`cluster_id` VARCHAR(255) NULL DEFAULT NULL COMMENT '集群ID' ,
	`status` TINYINT(1) NULL DEFAULT NULL COMMENT '执行状态:1:提交；2：运行中；3：成功；4：失败；5：手动取消',
	`write_num` BIGINT(20) NULL DEFAULT NULL COMMENT '任务已经写入的记录数',
	`start_time` DATETIME NULL DEFAULT NULL COMMENT '开始时间',
	`end_time` DATETIME NULL DEFAULT NULL COMMENT '结束时间',
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;