/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao;

import com.chinamobile.cmss.lakehouse.common.enums.ResourceSizeEnum;
import com.chinamobile.cmss.lakehouse.dao.entity.LakehouseClusterInfoEntity;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface LakehouseClusterInfoDao extends JpaRepository<LakehouseClusterInfoEntity, Long>, JpaSpecificationExecutor<LakehouseClusterInfoEntity> {

    List<LakehouseClusterInfoEntity> findByStatusIn(List<String> status);

    LakehouseClusterInfoEntity findByInstanceId(String instanceId);

    LakehouseClusterInfoEntity findByInstance(String clusterName);

    LakehouseClusterInfoEntity findByUserIdAndInstance(String userId, String instance);

    List<LakehouseClusterInfoEntity> findByStatusAndClusterType(String status, String clusterType);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update LakehouseClusterInfoEntity w set w.taskParallelize= ?2 where w.instanceId = ?1")
    void updateParallel(String instanceId, int parallel);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update LakehouseClusterInfoEntity w set w.computeResourceSize= ?2 where w.instanceId = ?1")
    void updateClusterSize(String instanceId, ResourceSizeEnum sizeEnum);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update LakehouseClusterInfoEntity w set w.status= ?2 where w.instanceId = ?1")
    void updateStatus(String instanceId, String status);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update LakehouseClusterInfoEntity p set p.status=:runningStatus where p.instanceId in (:collection)")
    int updateListStatus(@Param("runningStatus") String runningStatus,
                         @Param("collection") Collection<String> collection);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update LakehouseClusterInfoEntity p set p.expireTime=?2 where p.instanceId=?1")
    void updateExpireTimeByInstanceId(String instanceId, Date expireTime);

    List<LakehouseClusterInfoEntity> findAllByUserIdAndEngineType(String userId, String clusterType);
}
