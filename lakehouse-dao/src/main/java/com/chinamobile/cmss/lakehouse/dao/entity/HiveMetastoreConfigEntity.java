/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity
@Table(name = "hive.table_params")
public class HiveMetastoreConfigEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private MetastoreEmbeddedId id;

    @Column(name = "PARAM_VALUE")
    private String paramValue;

    public String getParamKey() {
        return id.getParamKey();
    }

    @Embeddable
    @Data
    public static class MetastoreEmbeddedId implements Serializable {
        private static final long serialVersionUID = 1L;

        @Column(name = "TBL_ID")
        private int tblId;

        @Column(name = "PARAM_KEY")
        private String paramKey;
    }
}


