/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao;

import com.chinamobile.cmss.lakehouse.dao.entity.SparkFilenameTaskEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SparkFilenameTaskDao extends JpaRepository<SparkFilenameTaskEntity, Long>, JpaSpecificationExecutor<SparkFilenameTaskEntity> {

    SparkFilenameTaskEntity findByTaskId(String taskId);

    @Modifying
    @Query("update SparkFilenameTaskEntity set filename = ?2 where fileId = ?1")
    void updateFilenameByFileId(String id, String filename);

    @Query("select s.taskId from SparkFilenameTaskEntity s where s.filename like %?1%")
    List<String> findByFilename(String name);

    @Query("from SparkFilenameTaskEntity where taskId= ?1 and createUserId = ?2")
    SparkFilenameTaskEntity findByTaskIdAndUserId(String taskId, String userId);

    @Query("select s.taskId from SparkFilenameTaskEntity s where s.scheduleModel= ?1 and s.createUserId = ?2")
    List<String> findByScheduleModelAndUserId(String scheduleModel, String userId);

    @Query("select s.taskId from SparkFilenameTaskEntity s where s.relatedTaskId like %?1% and s.createUserId = ?2")
    List<String> findByRelatedTaskIdAndUserId(String relatedTaskId, String userId);

    @Query("select s.taskId from SparkFilenameTaskEntity s where s.relatedTaskName like %?1% and s.createUserId = ?2")
    List<String> findByRelatedTaskNameAndUserId(String relatedTaskName, String userId);

    @Query("select s.taskId from SparkFilenameTaskEntity s where s.scheduleModel is null and s.createUserId = ?1")
    List<String> findTaskIdByUserId(String userId);
}
