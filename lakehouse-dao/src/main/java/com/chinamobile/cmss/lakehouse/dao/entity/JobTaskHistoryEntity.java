/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import com.chinamobile.cmss.lakehouse.common.enums.JobRunningStatusEnum;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "job_task_history")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobTaskHistoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "job_history_id")
    private Long jobHistoryId;

    @Column(name = "flink_web_url")
    private String flinkWebUrl;

    @Column(name = "flinkx_config")
    private String flinkxConfig;

    @Column(name = "`status`")
    private JobRunningStatusEnum jobRunningStatus;

    @Column(name = "cluster_id")
    private String clusterId;

    @Column(name = "write_num")
    private Long writeNum;

    @ApiModelProperty("开始时间")
    @Column(name = "start_time")
    private Date startTime;

    @ApiModelProperty("结束时间")
    @Column(name = "end_time")
    private Date endTime;
}
