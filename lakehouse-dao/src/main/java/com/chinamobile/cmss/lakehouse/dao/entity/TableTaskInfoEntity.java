/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import com.chinamobile.cmss.lakehouse.common.enums.ExecuteTaskType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

@Data
@Entity
@Table(name = "table_task_inf", indexes = {
    @Index(name = "table_task_ref_TASK_ID_IDX", columnList = "TASK_ID"),
    @Index(name = "table_task_ref_DB_NAME_IDX", columnList = "DB_NAME"),
    @Index(name = "table_task_ref_TB_NAME_IDX", columnList = "TB_NAME"),
    @Index(name = "table_task_ref_CREATE_TIME_IDX", columnList = "CREATE_TIME"),
    @Index(name = "table_task_ref_DB_NAME_TB_NAME_UNIQUE_IDX", columnList = "DB_NAME,TB_NAME", unique = true)
})
@DynamicUpdate
public class TableTaskInfoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "bigint(20)")
    private long id;

    @Column(name = "TASK_TYPE", columnDefinition = "smallint(2)")
    @Enumerated(EnumType.ORDINAL)
    private ExecuteTaskType taskType;

    @Column(name = "TASK_ID", columnDefinition = "varchar(256)")
    private String taskId;

    @Column(name = "DB_NAME", columnDefinition = "varchar(256)")
    private String dbName;

    @Column(name = "TB_NAME", columnDefinition = "varchar(256)")
    private String tableName;

    @Column(name = "CREATOR", columnDefinition = "varchar(256)")
    private String creator;

    @Column(name = "CREATE_TIME", columnDefinition = "int(11)")
    private Integer createTime;
}
