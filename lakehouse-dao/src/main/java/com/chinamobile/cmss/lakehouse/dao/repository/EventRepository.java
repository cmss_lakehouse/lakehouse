/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.repository;

import com.chinamobile.cmss.lakehouse.dao.entity.EventEntity;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<EventEntity, Long> {

    List<EventEntity> findByInstanceIdAndEventAndTimeBetween(String instanceId, String event, Date start,
                                                             Date end);

    List<EventEntity> findByInstanceIdAndTimeBeforeOrderByTimeDesc(String instanceId, Date start);

    List<EventEntity> findByInstanceIdAndTimeBetweenOrderByTimeDesc(String instanceId, Date start,
                                                                    Date end);

    List<EventEntity> findByInstanceIdAndTimeBeforeOrderByTimeAsc(String instanceId, Date currentTime);

}
