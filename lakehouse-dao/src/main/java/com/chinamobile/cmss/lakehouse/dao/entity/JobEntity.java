/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import com.chinamobile.cmss.lakehouse.common.enums.JobRunningStatusEnum;
import com.chinamobile.cmss.lakehouse.common.enums.JobTypeEnum;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "job")
@SQLDelete(sql = "UPDATE job SET deleted = 1 where id = ?")
@Where(clause = "deleted = 0")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "name")
    @ApiModelProperty("任务名称")
    private String name;

    @Column(name = "description")
    @ApiModelProperty("任务描述")
    private String description;

    @ApiModelProperty("迁移任务类型，0：离线同步，1：实时同步")
    @Column(name = "job_type")
    @Enumerated(EnumType.ORDINAL)
    private JobTypeEnum jobType;

    @ApiModelProperty("数据源ID，来自datasource的ID")
    @Column(name = "source_id")
    private Long sourceId;

    @ApiModelProperty("数据源配置")
    @Column(name = "source_param")
    private String sourceParam;

    @ApiModelProperty("数据目标ID，来自datasource的ID")
    @Column(name = "target_id")
    private Long targetId;

    @ApiModelProperty("数据目标配置")
    @Column(name = "target_param")
    private String targetParam;

    @Column(name = "instance_id")
    @ApiModelProperty("数据源对应的实例id")
    private String instanceId;

    @ApiModelProperty("任务运行状态: RUNNING(0)执行中,SUCCEED(1)成功,FAILURE(2)失败,QUEUING(3)队列中,INTERRUPTED(4)已终止")
    @Column(name = "running_status")
    @Enumerated(EnumType.ORDINAL)
    private JobRunningStatusEnum runningStatus;

    @ApiModelProperty("调度配置")
    @JoinColumn(name = "schedule_info_id", referencedColumnName = "id")
    @OneToOne
    @JsonIgnoreProperties({"job"})
    private ScheduleInfoEntity scheduleInfo;
}
