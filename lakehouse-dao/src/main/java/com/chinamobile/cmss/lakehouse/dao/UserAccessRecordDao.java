/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao;

import com.chinamobile.cmss.lakehouse.dao.entity.UserAccessRecordEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface UserAccessRecordDao extends JpaRepository<UserAccessRecordEntity, Long> {

    UserAccessRecordEntity findByUserIdAndInstance(String userId, String instance);

    UserAccessRecordEntity findByUserIdAndInstanceAndAccess(String userId, String instance,
                                                            String access);

    UserAccessRecordEntity findByUserNameAndInstanceAndAccess(String userName, String instance,
                                                              String access);

    List<UserAccessRecordEntity> findByInstance(String instance);

    List<UserAccessRecordEntity> findByUserNameLikeAndInstance(String name, String instance);

    List<UserAccessRecordEntity> findByUserName(String name);

    @Query(
        value = "select userAccessTbl.instance from UserAccessRecordEntity userAccessTbl where userAccessTbl.userId = ?1 and userAccessTbl.access = ?2")
    List<String> findClusterByUserIdAndAccess(String userId, String access);

    @Query(
        value = "select userAccessTbl.instance from UserAccessRecordEntity userAccessTbl where userAccessTbl.userId = ?1 and userAccessTbl.access = ?2 and userAccessTbl.instance like ?3")
    List<String> findClusterByUserIdAndAccessAndInstanceLike(String userId, String access,
                                                             String instanceLike);

    @Transactional
    void deleteByInstance(String instance);
}
