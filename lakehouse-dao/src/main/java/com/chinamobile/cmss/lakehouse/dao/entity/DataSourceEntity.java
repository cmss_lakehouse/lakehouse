/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import com.chinamobile.cmss.lakehouse.common.enums.DataSourceTypeEnum;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "data_source")
@SQLDelete(sql = "UPDATE data_source SET deleted = 1 where id = ?")
@Where(clause = "deleted = 0")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DataSourceEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "name", nullable = false)
    @ApiModelProperty("数据源名称")
    private String name;

    @Column(name = "data_source_type", nullable = false)
    @ApiModelProperty("数据源分类")
    @Enumerated(EnumType.STRING)
    private DataSourceTypeEnum dataSourceType;

    @Column(name = "ip")
    @ApiModelProperty("数据源ip")
    private String ip;

    @Column(name = "port")
    @ApiModelProperty("数据源port")
    private Integer port;

    @ApiModelProperty("数据源url,复杂数据源url采用此配置如kafka等")
    @Column(name = "url", columnDefinition = "varchar(512)")
    private String url;

    @Column(name = "username", nullable = false)
    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("数据库密码")
    @Column(name = "password", columnDefinition = "varchar(1024)")
    private String password;

    @ApiModelProperty("数据源扩展配置")
    @Column(name = "extend_param")
    private String extendParam;
}
