/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import com.chinamobile.cmss.lakehouse.common.enums.FailActionEnum;
import com.chinamobile.cmss.lakehouse.common.enums.ScheduleModelEnum;
import com.chinamobile.cmss.lakehouse.common.enums.ScheduleStatusEnum;

import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "schedule_info")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ScheduleInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "schedule_model")
    @Enumerated(EnumType.ORDINAL)
    @ApiModelProperty("调度方式：0:周期运行，1：单次调度")
    private ScheduleModelEnum scheduleModel;

    @Column(name = "schedule_status")
    @Enumerated(EnumType.ORDINAL)
    @ApiModelProperty("调度状态，1：开启，0：停止(默认)")
    private ScheduleStatusEnum scheduleStatus = ScheduleStatusEnum.OFF;

    @Column(name = "duration_unit")
    @Enumerated(EnumType.ORDINAL)
    @ApiModelProperty("调度周期单位，MINUTES(4):分钟，HOURS(5):小时，DAYS(7):天，WEEKS(8):周，MONTHS(9):月")
    private ChronoUnit durationUnit;

    @Column(name = "duration_value")
    @ApiModelProperty("调度周期")
    private Integer durationValue;

    @Column(name = "fail_action")
    @Enumerated(EnumType.ORDINAL)
    @ApiModelProperty("任务失败策略，0: 单次任务失败后，不再执行后续的调度任务,1: 单次任务失败后，忽略失败，并执行下一次调度任务")
    private FailActionEnum failAction = FailActionEnum.FAILED_ABORT;

    @Column(name = "cron_trigger")
    @ApiModelProperty("由配置的调度时间生成cron表达式")
    private String cronTrigger;

    @Column(name = "schedule_start_time")
    @ApiModelProperty("调度开始时间")
    private Date scheduleStartTime;

    @Column(name = "schedule_end_time")
    @ApiModelProperty("调度结束时间")
    private Date scheduleEndTime;
}
