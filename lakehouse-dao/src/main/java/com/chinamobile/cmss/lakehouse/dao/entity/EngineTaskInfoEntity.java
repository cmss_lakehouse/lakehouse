/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import com.chinamobile.cmss.lakehouse.common.enums.TaskStatusTypeEnum;
import com.chinamobile.cmss.lakehouse.dao.converter.ListToStringConverter;
import com.chinamobile.cmss.lakehouse.dao.converter.MapToJsonConverter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "engine_task_info")
public class EngineTaskInfoEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "instance")
    private String instance;

    @Column(name = "db_name")
    private String dbName;

    @Column(name = "task_id")
    private String taskId;

    @Column(name = "driver_pod_id")
    private String driverPodId;

    @CreatedDate
    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "submit_time")
    private Date submitTime;

    @LastModifiedDate
    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "finish_time")
    private Date finishTime;

    @Column(name = "canceled", columnDefinition = "bit(1) default 1")
    private boolean canceled;

    @Column(name = "create_user")
    private String createUser;

    @Column(name = "submit_user")
    private String submitUser;

    @Column(name = "submit_user_id")
    private String submitUserId;

    @Column(name = "sql_content")
    private String sqlContent;

    @Column(name = "task_result_redis_key")
    private String taskResultRedisKey;

    @Column(name = "task_query_metastore_url")
    private String queryMetastoreURI;

    @Column(name = "task_query_metastore_catalog")
    private String queryMetastoreCatalog;

    @Column(name = "task_query_redis_host")
    private String queryRedisHost;

    @Column(name = "task_query_redis_port")
    private String queryRedisPort;

    @Column(name = "task_query_redis_key")
    private String queryRedisKey;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private TaskStatusTypeEnum statusTypeEnum;

    /**
     * means retry times, max 3
     */
    @Column(name = "retry_times", columnDefinition = "int(1) default 0")
    private Integer retryTimes = 1;

    @Column(name = "log_path")
    private String logPath;

    @Column(name = "engine_type")
    private String engineType;

    @Column(name = "task_type")
    private String taskType;

    @Column(name = "wait_time")
    private Long waitTime;

    @Column(name = "run_time")
    private Long runTime;

    @Column(name = "result_rows")
    private Integer resultRows;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "main_class")
    private String mainClass;

    @Convert(converter = ListToStringConverter.class)
    @Column(name = "engine_args")
    private List<String> engineArgs;

    @Convert(converter = MapToJsonConverter.class)
    @Column(name = "engine_config", columnDefinition = "TEXT")
    private Map<String, String> engineConfig;

    @Version
    private Integer version;
}
