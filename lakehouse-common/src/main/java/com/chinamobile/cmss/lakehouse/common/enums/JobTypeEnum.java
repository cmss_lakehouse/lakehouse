/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum JobTypeEnum {
    /**
     * 0：离线同步
     */
    OFFLINE,
    /**
     * 1: 实时同步
     */
    REALTIME;

    public static JobTypeEnum convert(int index) {
        JobTypeEnum enumValue;
        switch (index) {
            case 0:
                enumValue = OFFLINE;
                break;
            case 1:
                enumValue = REALTIME;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + index);
        }

        return enumValue;
    }
}
