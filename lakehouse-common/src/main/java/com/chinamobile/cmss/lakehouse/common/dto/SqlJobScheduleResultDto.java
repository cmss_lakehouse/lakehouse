/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto;

import com.chinamobile.cmss.lakehouse.common.enums.TaskStatusTypeEnum;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SqlJobScheduleResultDto {
    /**
     * task id
     */
    private String taskId;
    /**
     * file name
     */
    private String name;
    /**
     * executed sql
     */
    private String sqlContent;
    /**
     * task type
     */
    private String taskType;
    /**
     * engine type
     */
    private String engineType;
    /**
     * hive db
     */
    private String dbName;
    /**
     * status
     */
    private TaskStatusTypeEnum status;
    /**
     * instance name
     */
    private String instance;
    /**
     * submit time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date submitTime;
    /**
     * update time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    /**
     * finish task time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date finishTime;
    /**
     * duration time
     */
    private Long runTime;
    /**
     * wating time
     */
    private Long waitTime;
    /**
     * submit user
     */
    private String submitUser;

    /**
     * spark作业Id
     */
    private String sparkTaskId;

    @ApiModelProperty("scheduleModel")
    private String scheduleModel;

    @ApiModelProperty("relatedTaskId")
    private String relatedTaskId;

    @ApiModelProperty("relatedTaskName")
    private String relatedTaskName;

    @ApiModelProperty("sparkContent")
    private String sparkContent;
}
