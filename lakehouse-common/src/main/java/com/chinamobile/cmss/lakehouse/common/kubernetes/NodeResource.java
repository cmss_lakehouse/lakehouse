/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.kubernetes;

import java.math.BigDecimal;

import io.kubernetes.client.openapi.models.V1Node;
import lombok.Data;
import lombok.ToString;

/**
 * Node Resources:
 *
 * Allocatable: it is actually available resources which is not equal to Capacity.
 * Requests: calculated by Non-terminated Pods.
 * Limits: calculated by Non-terminated Pods.
 */
@Data
@ToString
public class NodeResource {
    private BigDecimal cpuAllocatable;
    private BigDecimal memoryAllocatable;
    private BigDecimal cpuRequests;
    private BigDecimal memoryRequests;
    private BigDecimal cpuLimits;
    private BigDecimal memoryLimits;
    private V1Node v1Node;

    public BigDecimal getCpuRemaining() {
        return this.cpuAllocatable.subtract(this.cpuLimits);
    }

    public BigDecimal getMemoryRemaining() {
        return this.memoryAllocatable.subtract(this.memoryLimits);
    }
}