/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ExecuteTaskType {

    SQL_TASK("SQL"),
    DATA_SYNC("数据同步"),
    DATA_DISCOVERY("元数据发现"),
    UNKNOWN("-");

    private static final Map<String, ExecuteTaskType> TASK_TYPE_MAP = Arrays.stream(ExecuteTaskType.values())
        .collect(Collectors.toMap(e -> e.getCnName(), e -> e));

    private static final String CN_NAMES = TASK_TYPE_MAP.values().stream()
        .map(v -> v.cnName).collect(Collectors.joining(",", "[", "]"));

    private final String cnName;

    ExecuteTaskType(String cnName) {
        this.cnName = cnName;
    }

    public static ExecuteTaskType forValues(@JsonProperty("refTaskType") String cnName) {
        cnName = cnName.trim();
        if (!TASK_TYPE_MAP.containsKey(cnName)) {
            StringBuffer msg = new StringBuffer();
            msg.append("unknown taskType ").append(cnName)
                .append(" taskType must in")
                .append(CN_NAMES);
            throw new IllegalArgumentException(msg.toString());
        }
        return TASK_TYPE_MAP.get(cnName);
    }

    @JsonValue
    public String getCnName() {
        return cnName;
    }
}
