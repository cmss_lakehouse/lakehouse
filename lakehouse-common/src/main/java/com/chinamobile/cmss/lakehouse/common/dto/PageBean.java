/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PageBean<T> {

    private int offset;

    private int limit;

    private int totalPage;

    private long totalElements;

    private List<T> resultList;

    public PageBean() {

    }

    public PageBean(List<T> resultList, int totalPage, long totalElements, Pageable pageable) {
        if (resultList == null) {
            return;
        }
        this.offset = pageable.getPageNumber() + 1;
        this.limit = pageable.getPageSize();
        this.totalPage = totalPage;
        this.totalElements = totalElements;
        this.resultList = resultList;
    }

    public PageBean(Page<T> page, Pageable pageable) {
        if (page == null) {
            return;
        }
        this.offset = pageable.getPageNumber() + 1;
        this.limit = pageable.getPageSize();
        this.totalPage = page.getTotalPages();
        this.totalElements = page.getTotalElements();
        this.resultList = page.getContent();
    }

}