/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.constants;

public class CommonVar {

    /**
     * hive
     */
    public static final String HIVE_DB_TYPE = "hive";

    /**
     * Char Code
     */
    public static final String UTF_8 = "UTF-8";

    public static final String KEY_REQUEST_ID = "requestId";

    /**
     * SQL Console
     */
    public static final String REDIS_KEY_PREFIX = "cluster:running:";
    public static final String REDIS_KEY_PATTERN = "cluster:running:*";

    public static final String NETWORK_ID_PREFIX = "network-";
    public static final String NETWORK_GROUP_ID_PREFIX = "network-group-";

    public static final String RAM_TOKEN = "ram_token";

    public static final String USER_ID = "user_id";

    public static final String RAM_USERID_ACTION = "UNIDENTIFY";
}
