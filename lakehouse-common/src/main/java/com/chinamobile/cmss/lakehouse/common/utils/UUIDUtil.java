/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.utils;

import static com.chinamobile.cmss.lakehouse.common.constants.CommonVar.NETWORK_GROUP_ID_PREFIX;
import static com.chinamobile.cmss.lakehouse.common.constants.CommonVar.NETWORK_ID_PREFIX;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class UUIDUtil {

    public static String generateUuid() {
        return UUID.randomUUID().toString();
    }

    public static String randomName(int numChars) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numChars; i++) {
            sb.append((char) (ThreadLocalRandom.current().nextInt(26) + 'a'));
        }
        return sb.toString();
    }

    public static String generateNetworkGroupUuid() {
        return NETWORK_GROUP_ID_PREFIX + UUID.randomUUID().toString();
    }

    public static String generateNetworkUuid() {
        return NETWORK_ID_PREFIX + UUID.randomUUID().toString();
    }

    public static String generateParentTaskUuid() {
        return UUID.randomUUID().toString();
    }

    public static String generateAccessKey() {
        return UUID.randomUUID().toString().toUpperCase().replaceAll("-", "");
    }

}
