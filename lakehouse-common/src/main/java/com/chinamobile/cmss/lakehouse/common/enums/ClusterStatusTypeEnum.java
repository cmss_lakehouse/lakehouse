/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.enums;

public enum ClusterStatusTypeEnum {

    /**
     * accepted
     */
    ACCEPTED("accepted", "集群初始化入库"),
    /**
     * deploying
     */
    DEPLOYING("deploying", "正在部署集群"),
    /**
     * deploy failed
     */
    DEPLOY_FAILED("deploy_failed", "部署集群失败"),
    /**
     * creating
     */
    CREATING("creating", "正在创建中"),
    /**
     * create failed
     */
    CREATE_FAILED("create_failed", "创建失败"),
    /**
     * configuring
     */
    CONFIGURING("configuring", "路径配置中"),
    /**
     * configured
     */
    CONFIGURED("configured", "配置成功"),

    /**
     * running
     */
    RUNNING("running", "正常运行中"),
    /**
     * error
     */
    ERROR("error", "运行异常"),

    /**
     * releasing
     */
    RELEASING("releasing", "释放中"),
    /**
     * released
     */
    RELEASED("released", "已释放"),
    /**
     * deleted
     */
    DELETED("deleted", "已删除"),
    /**
     * frozen
     */
    FROZEN("frozen", "冻结"),

    /**
     * resizing
     */
    RESIZING("resizing", "变更中"),
    /**
     * resized
     */
    RESIZED("resized", "变更成功"),
    /**
     * resize failed
     */
    RESIZE_FAILED("resize_failed", "变更失败");

    String status;
    String description;

    ClusterStatusTypeEnum(String status, String description) {
        this.status = status;
        this.description = description;
    }

    public String getStatus() {
        return status;
    }
}
