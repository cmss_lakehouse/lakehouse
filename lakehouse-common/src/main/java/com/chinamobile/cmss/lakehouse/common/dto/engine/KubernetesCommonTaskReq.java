/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto.engine;

import com.chinamobile.cmss.lakehouse.common.enums.TaskType;

import java.util.List;

import lombok.Data;

@Data
public class KubernetesCommonTaskReq extends KubernetesTaskReq {
    private String mainClass;
    private String jarPath;
    private List<String> engineArgs;
    private String metastoreUris;
    private String defaultDatabase = "default";
    private TaskType type = TaskType.CONSOLE;

    @Override
    public String toString() {
        return String.format(
            "master: %s mainClass: %s jarPath: %s "
                + "namespace: %s user: %s engineArgs: %s database: %s "
                + "driverMemory: %s executorMemory: %s driverCores: %s executorCores: %s",
            getMaster(), mainClass, jarPath, getNamespace(), getProxyUser(), engineArgs, defaultDatabase,
            getDriverMemory(), getExecutorMemory(), getDriverCores(), getExecutorCores());
    }
}
