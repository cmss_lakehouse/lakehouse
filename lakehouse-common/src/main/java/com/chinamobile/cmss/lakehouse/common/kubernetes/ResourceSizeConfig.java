/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.kubernetes;

import com.chinamobile.cmss.lakehouse.common.Constants;
import com.chinamobile.cmss.lakehouse.common.enums.ResourceSizeEnum;
import com.chinamobile.cmss.lakehouse.common.utils.PropertyUtils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class ResourceSizeConfig {

    public static final String[] resourceSmallSize = PropertyUtils.getArray(Constants.RESOURCE_SIZE_SMALL, ",");
    public static final String[] resourceMediumSize = PropertyUtils.getArray(Constants.RESOURCE_SIZE_MEDIUM, ",");
    public static final String[] resourceLargeSize = PropertyUtils.getArray(Constants.RESOURCE_SIZE_LARGE, ",");
    public static final String[] resourceXLargeSize = PropertyUtils.getArray(Constants.RESOURCE_SIZE_XLARGE, ",");
    public static final String resourceStrategy = PropertyUtils.getString(Constants.RESOURCE_STRATEGY, "fifo");
    public static final String resourceManager = PropertyUtils.getString(Constants.RESOURCE_MANAGER, "yunikornResourceManager");
    public static final Integer resourceScale = PropertyUtils.getInt(Constants.RESOURCE_SCALE, 4);

    public static ComputeResource getResource(ResourceSizeEnum resourceSize) {
        Map<ResourceSizeEnum, ComputeResource> config = new HashMap<>(4);
        config.put(ResourceSizeEnum.valueOf("SMALL"),
            ComputeResource.builder().cpu(Integer.parseInt(resourceSmallSize[0].trim()))
                .memory(Integer.parseInt(resourceSmallSize[1].trim())).build());
        config.put(ResourceSizeEnum.valueOf("MEDIUM"),
            ComputeResource.builder().cpu(Integer.parseInt(resourceMediumSize[0].trim()))
                .memory(Integer.parseInt(resourceMediumSize[1].trim())).build());
        config.put(ResourceSizeEnum.valueOf("LARGE"),
            ComputeResource.builder().cpu(Integer.parseInt(resourceLargeSize[0].trim()))
                .memory(Integer.parseInt(resourceLargeSize[1].trim())).build());
        config.put(ResourceSizeEnum.valueOf("XLARGE"),
            ComputeResource.builder().cpu(Integer.parseInt(resourceXLargeSize[0].trim()))
                .memory(Integer.parseInt(resourceXLargeSize[1].trim())).build());
        return config.get(resourceSize);
    }
}
