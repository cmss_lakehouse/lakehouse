/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CommandBuilder<T extends CommandBuilder> {
    protected final List<String> appArgs;
    protected final Map<String, String> childEnv;
    protected final Map<String, String> conf;
    protected String appResource;

    public CommandBuilder() {
        this.appArgs = new ArrayList<>();
        this.childEnv = new HashMap<>();
        this.conf = new HashMap<>();
    }

    public T setConf(String key, String value) {
        this.conf.put(key, value);
        return self();
    }

    public T setConf(Map<String, String> conf1) {
        this.conf.putAll(conf1);
        return self();
    }

    public T setAppResource(String jar) {
        this.appResource = jar;
        return self();
    }

    public T args(String... args) {
        return args(Arrays.asList(args));
    }

    public T args(List<String> args) {
        this.appArgs.addAll(args);
        return self();
    }

    public abstract List<String> buildSubmitArgs();

    public abstract List<String> buildKillArgs(String podId);

    public Map<String, String> childEnv() {
        return this.childEnv;
    }

    protected abstract T self();
}
