/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto.engine;

import com.chinamobile.cmss.lakehouse.common.enums.HttpStatus;

import lombok.Data;

@Data
public class LakehouseResponse<T> {
    private static final long serialVersionUID = 1L;

    private Integer code;

    private String message;

    private T data;

    private LakehouseResponse() {
    }

    public LakehouseResponse(HttpStatus status, T data) {
        this.code = status.getCode();
        this.message = status.getDescription();
        this.data = data;
    }

    private void setHttpStatus(HttpStatus status) {
        this.code = status.getCode();
        this.message = status.getDescription();
    }

    public static LakehouseResponse success() {
        LakehouseResponse result = new LakehouseResponse();
        result.setHttpStatus(HttpStatus.OK);
        return result;
    }

    public static <T> LakehouseResponse success(T data) {
        return new LakehouseResponse(HttpStatus.OK, data);
    }

    public static LakehouseResponse fail(Integer code, String message) {
        LakehouseResponse result = new LakehouseResponse();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

    public static <T> LakehouseResponse fail(Integer code, String message, T data) {
        LakehouseResponse result = new LakehouseResponse();
        result.setCode(code);
        result.setMessage(message);
        result.setData(data);
        return result;
    }

    public static LakehouseResponse fail(HttpStatus status) {
        LakehouseResponse result = new LakehouseResponse();
        result.setHttpStatus(status);
        return result;
    }

}
