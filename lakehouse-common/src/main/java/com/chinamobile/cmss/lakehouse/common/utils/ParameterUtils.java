/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.utils;

public class ParameterUtils {
    private static final String[] SPECIAL_STR_ARR = {"%", "_"};

    /**
     * Replace the special characters in the SQL like operation: '%', '_' to '\%', '\_'
     *
     * @param condition the replaced like conditional character
     * @return the final like conditional character
     */
    public static String replaceSpecialChars(String condition) {
        for (String specialStr : SPECIAL_STR_ARR) {
            if (condition.contains(specialStr)) {
                condition = condition.replaceAll(specialStr, "\\\\" + specialStr);
            }
        }
        return condition;
    }
}
