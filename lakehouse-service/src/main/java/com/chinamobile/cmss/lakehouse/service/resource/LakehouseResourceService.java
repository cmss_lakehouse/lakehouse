/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.service.resource;

import com.chinamobile.cmss.lakehouse.common.dto.ClusterCreateStatus;
import com.chinamobile.cmss.lakehouse.common.dto.LakehouseCheckRequest;
import com.chinamobile.cmss.lakehouse.common.dto.LakehouseCreateRequest;
import com.chinamobile.cmss.lakehouse.common.dto.LakehouseReleaseRequest;

import java.util.concurrent.CompletableFuture;

public interface LakehouseResourceService {

    boolean createLakehouseResource(LakehouseCreateRequest request);

    CompletableFuture<Boolean> createLakehouseResourceAsync(LakehouseCreateRequest request);

    boolean releaseLakehouseResource(LakehouseReleaseRequest req);

    CompletableFuture<Boolean> releaseLakehouseResourceAsync(LakehouseReleaseRequest req);

    ClusterCreateStatus checkClusterConfigOk(LakehouseCheckRequest req);

    boolean checkClusterHealthStatus(String clusterName) throws Exception;
}
