/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.service.resource.impl;

import com.chinamobile.cmss.lakehouse.common.dto.ClusterCreateStatus;
import com.chinamobile.cmss.lakehouse.common.dto.LakehouseCheckRequest;
import com.chinamobile.cmss.lakehouse.common.dto.LakehouseCreateRequest;
import com.chinamobile.cmss.lakehouse.common.dto.LakehouseReleaseRequest;
import com.chinamobile.cmss.lakehouse.core.client.InformerClient;
import com.chinamobile.cmss.lakehouse.core.pipeline.ClusterCheckContext;
import com.chinamobile.cmss.lakehouse.core.pipeline.ClusterCreateContext;
import com.chinamobile.cmss.lakehouse.core.pipeline.ClusterReleaseContext;
import com.chinamobile.cmss.lakehouse.service.pipeline.PipelineExecutor;
import com.chinamobile.cmss.lakehouse.service.resource.LakehouseResourceService;

import java.util.concurrent.CompletableFuture;

import io.kubernetes.client.openapi.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LakehouseResourceServiceImpl implements LakehouseResourceService {

    @Autowired
    PipelineExecutor pipelineExecutor;

    @Autowired
    private InformerClient informerClient;

    @Override
    @Async(value = "LakehouseScheduleExecutor")
    public CompletableFuture<Boolean> createLakehouseResourceAsync(LakehouseCreateRequest request) {
        boolean created = createLakehouseResource(request);
        return CompletableFuture.completedFuture(created);
    }

    @Override
    public boolean createLakehouseResource(LakehouseCreateRequest request) {
        return pipelineExecutor.createSync(new ClusterCreateContext(request));
    }

    @Override
    public boolean releaseLakehouseResource(LakehouseReleaseRequest req) {
        return pipelineExecutor.releaseSync(new ClusterReleaseContext(req));
    }

    @Override
    @Async(value = "LakehouseScheduleExecutor")
    public CompletableFuture<Boolean> releaseLakehouseResourceAsync(LakehouseReleaseRequest req) {
        boolean released = releaseLakehouseResource(req);
        return CompletableFuture.completedFuture(released);
    }

    @Override
    public ClusterCreateStatus checkClusterConfigOk(LakehouseCheckRequest req) {
        return pipelineExecutor.checkStatus(new ClusterCheckContext(req));
    }

    @Override
    public boolean checkClusterHealthStatus(String clusterName) throws ApiException {
        return informerClient.checkResourceHealthStatus(clusterName);
    }
}
