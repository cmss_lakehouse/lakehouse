/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.service.engine;

import com.chinamobile.cmss.lakehouse.common.dto.ExecuteSQLBean;
import com.chinamobile.cmss.lakehouse.common.dto.engine.LakehouseResponse;

public interface SparkTaskService {

    public abstract String acceptTask(ExecuteSQLBean req);

    public abstract void submitTask();

    public abstract void monitorTask();

    public abstract void recoverTask();

    public abstract LakehouseResponse allocateJdbcResource(String clusterName);

    public abstract LakehouseResponse releaseJdbcResource(String clusterName);
}
