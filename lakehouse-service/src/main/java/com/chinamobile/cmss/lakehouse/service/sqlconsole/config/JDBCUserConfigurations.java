/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.service.sqlconsole.config;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class JDBCUserConfigurations {
    private final Map<String, Statement> taskIdStatementMap;

    public JDBCUserConfigurations() {
        taskIdStatementMap = new HashMap<>();
    }

    public void saveStatement(String taskId, Statement statement) {
        taskIdStatementMap.put(taskId, statement);
    }

    public void cancelStatement(String taskId) throws SQLException {
        taskIdStatementMap.get(taskId).cancel();
    }

    public void removeStatement(String taskId) {
        taskIdStatementMap.remove(taskId);
    }
}
