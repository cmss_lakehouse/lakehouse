/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.service;

import com.chinamobile.cmss.lakehouse.api.dto.LakehouseInstanceBean;
import com.chinamobile.cmss.lakehouse.common.utils.Result;
import com.chinamobile.cmss.lakehouse.dao.entity.LakehouseClusterInfoEntity;
import com.chinamobile.cmss.lakehouse.dao.entity.UserEntity;

import java.util.List;
import java.util.Map;

public interface LakehouseInstanceService {

    Map<String, Object> createLakehouseInstance(String userId, LakehouseInstanceBean lakehouseInstanceBean);

    Map<String, Object> cancelLakehouseInstance(String userId, String instanceId);

    boolean isInstanceNameExists(String userId, String instanceName);

    Map<String, Object> getInstanceList(String userId, String engineType);

    Map<String, Object> getInstanceNames(String userId, String engineType);

    Map<String, Object> getInstanceListHealthStatus(List<LakehouseClusterInfoEntity> clusterInfoEntityList);

    void updateInstanceListStatus(String status, List<LakehouseClusterInfoEntity> clusterInfoEntityList);

    Result getInstanceList(UserEntity loginUser, String searchVal, Integer pageNo, Integer pageSize);

    Map<String, Object> getInstance(UserEntity loginUser, String instanceId);
}