/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.dto;

import com.chinamobile.cmss.lakehouse.common.enums.DataSourceTypeEnum;
import com.chinamobile.cmss.lakehouse.dao.entity.DataSourceEntity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DataSourceKey {
    @ApiModelProperty(value = "数据源所属大类", required = true)
    private DataSourceTypeEnum dataSourceType;

    @ApiModelProperty(value = "数据源url", required = true)
    private String url;

    @ApiModelProperty(value = "ip")
    private String ip;

    @ApiModelProperty(value = "端口")
    private Integer port;

    @ApiModelProperty(value = "数据源用户名")
    private String username;

    @ApiModelProperty(value = "数据源密码")
    private String password;

    public DataSourceKey(DataSourceEntity dataSource) {
        super();
        this.dataSourceType = dataSource.getDataSourceType();
        this.url = dataSource.getUrl();
        this.ip = dataSource.getIp();
        this.port = dataSource.getPort();
        this.username = dataSource.getUsername();
        this.password = dataSource.getPassword();
    }
}
