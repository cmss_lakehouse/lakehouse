/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.dto;

import com.chinamobile.cmss.lakehouse.common.enums.JobRunningStatusEnum;
import com.chinamobile.cmss.lakehouse.common.enums.JobTypeEnum;

import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "数据同步任务-创建")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobBean {
    private Long id;

    @ApiModelProperty("任务名称")
    private String name;

    @ApiModelProperty("任务描述")
    private String description;

    @ApiModelProperty("迁移任务类型，0：离线同步，1：实时同步")
    @Enumerated(EnumType.ORDINAL)
    private JobTypeEnum jobType;

    @ApiModelProperty("数据源ID，来自datasource的ID")
    private Long sourceId;

    @ApiModelProperty("数据源配置")
    private String sourceParam;

    @ApiModelProperty("数据目标ID，来自datasource的ID")
    private Long targetId;

    @ApiModelProperty("数据目标配置")
    private String targetParam;

    @ApiModelProperty("数据源对应的实例id")
    private String instanceId;

    @ApiModelProperty("任务运行状态: RUNNING(0)执行中,SUCCEED(1)成功,FAILURE(2)失败,QUEUING(3)队列中,INTERRUPTED(4)已终止")
    @Enumerated(EnumType.ORDINAL)
    private JobRunningStatusEnum runningStatus;

    @ApiModelProperty("调度配置")
    private ScheduleInfoBean scheduleInfo;

    @ApiModelProperty("创建人ID")
    private String createUserId;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新人ID")
    private String updateUserId;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("是否软删除，0：正常，1：软删除")
    private Boolean deleted = Boolean.FALSE;
}
