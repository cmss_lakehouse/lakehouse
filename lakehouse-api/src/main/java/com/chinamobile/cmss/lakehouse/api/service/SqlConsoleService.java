/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.service;

import com.chinamobile.cmss.lakehouse.api.dto.SqlQueryParamBean;
import com.chinamobile.cmss.lakehouse.common.dto.SqlJobQueryParamDto;
import com.chinamobile.cmss.lakehouse.dao.entity.UserEntity;

import java.util.List;
import java.util.Map;

public interface SqlConsoleService {

    /**
     * get formatted sql
     *
     * @param userId   userid
     * @param sqlQuery
     * @param dbType   db
     * @param instance
     * @return
     */
    Map<String, Object> getFormattedSql(String userId, String sqlQuery, String dbType, String instance);

    /**
     * execute sql
     *
     * @param loginUser         UserEntity
     * @param sqlQueryParamBean
     * @return
     */
    Map<String, Object> executeSql(UserEntity loginUser, SqlQueryParamBean sqlQueryParamBean);

    /**
     * abort Sql committed job
     *
     * @param jobIds
     * @return
     */
    Map<String, Object> abortSql(List<String> jobIds);

    /**
     * get query job exec result
     *
     * @param jobId      job id
     * @param engineType engine type
     * @return
     */
    Map<String, Object> getQueryResult(String jobId, String engineType);

    /**
     * get task list by pagination
     *
     * @param sqlJobQueryParamDto
     * @return
     */
    Map<String, Object> querySqlTaskInfo(SqlJobQueryParamDto sqlJobQueryParamDto);

}
