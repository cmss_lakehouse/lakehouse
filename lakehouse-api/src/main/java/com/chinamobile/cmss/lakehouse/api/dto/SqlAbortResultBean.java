/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SqlAbortResultBean {
    /**
     * JobId
     */
    private String jobId;

    /**
     * return commit success or not, some jobs may be aborted with some delays.
     * therefore abort option is Asynchronous.
     */
    private boolean commitSuccessed;

    /**
     * when job commit success, This field returns an empty string.
     * when job commit failed, this field returns error logs.
     */
    private String message;
}
