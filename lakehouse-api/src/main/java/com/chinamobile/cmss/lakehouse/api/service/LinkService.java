/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.service;

import com.chinamobile.cmss.lakehouse.api.dto.DataSourceBean;
import com.chinamobile.cmss.lakehouse.common.dto.flinkx.FieldConf;
import com.chinamobile.cmss.lakehouse.common.dto.flinkx.ReaderConfig;
import com.chinamobile.cmss.lakehouse.common.dto.flinkx.WriterConfig;
import com.chinamobile.cmss.lakehouse.common.enums.DataSourceTypeEnum;
import com.chinamobile.cmss.lakehouse.common.exception.BaseException;
import com.chinamobile.cmss.lakehouse.dao.entity.DataSourceEntity;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

public interface LinkService {

    /**
     * Validate dataSource
     * @param dataSource
     */
    default void validate(DataSourceBean dataSource) {
        if (StringUtils.isBlank(dataSource.getUsername())) {
            throw new BaseException("UserName can't be null.");
        }
    }

    /**
     * Append DB type jdbc url
     * @param dataSourceBean
     * @return
     */
    String appendUrl(DataSourceBean dataSourceBean);

    /**
     * Test dataSource connection
     * @param dataSourceEntity
     * @return
     */
    Boolean isConnected(DataSourceEntity dataSourceEntity);

    /**
     * Query dataSource table or path list
     * @param dataSourceEntity
     * @param param
     * @return
     */
    default List<String> queryTablePath(DataSourceEntity dataSourceEntity, String param) {
        return null;
    }

    /**
     * Get flinkx reader config
     * @param dataSourceEntity
     * @return
     */
    default ReaderConfig getReaderConfig(DataSourceEntity dataSourceEntity) {
        return null;
    }

    /**
     * Get flinkx writer config
     * @param dataSourceEntity
     * @return
     */
    default WriterConfig getWriterConfig(DataSourceEntity dataSourceEntity) {
        return null;
    }

    /**
     * Get flinkx reader name by datasource type
     * @param dataSourceType
     * @return
     */
    default String getReaderName(DataSourceTypeEnum dataSourceType) {
        String readerName;

        switch (dataSourceType) {
            case MYSQL:
                readerName = "mysqlreader";
                break;
            default:
                readerName = "mysqlreader";
                break;
        }

        return readerName;
    }

    /**
     * Get flinkx writer name by datasource type
     * @param dataSourceType
     * @return
     */
    default String getWriterName(DataSourceTypeEnum dataSourceType) {
        String writerName;
        switch (dataSourceType) {
            case MYSQL:
                writerName = "mysqlwriter";
                break;
            case S3:
                writerName = "s3writer";
                break;
            default:
                writerName = "mysqlwriter";
                break;
        }

        return writerName;
    }

    /**
     * Get table or file path schema
     * @param dataSourceType
     * @param param
     * @return
     */
    default String getTablePathSchema(DataSourceTypeEnum dataSourceType, String param) {
        return null;
    }

    /**
     * Get metadata of datasource
     * @param dataSourceEntity
     * @param table
     * @param param
     * @return
     */
    default List<FieldConf> getMetadata(DataSourceEntity dataSourceEntity, String table, String param) {
        return null;
    }

    /**
     * Update flinkx reader conf
     * @param readerConfig
     * @param tablePath
     * @param columns
     * @param sourceSchema
     */
    default void updateReaderConfig(ReaderConfig readerConfig, String tablePath, List<FieldConf> columns, String sourceSchema) {
    }

    /**
     * Check table or path exist
     * @param dataSourceEntity
     * @param schema
     * @param tablePath
     * @param userName
     * @return
     */
    default Boolean checkTablePathExist(DataSourceEntity dataSourceEntity, String schema, String tablePath, String userName) {
        return null;
    }

    /**
     * Create new table or file path
     * @param dataSourceEntity
     * @param schema
     * @param tablePath
     * @param userName
     * @param param
     * @param metaColumns
     * @return
     */
    default Boolean createNewTablePath(DataSourceEntity dataSourceEntity, String schema, String tablePath, String userName, String param, List<FieldConf> metaColumns) {
        return null;
    }

    /**
     * Update flinkx writer conf
     * @param writerConfig
     * @param tablePath
     * @param columns
     * @param schema
     */
    default void updateWriteConfig(WriterConfig writerConfig, String tablePath, List<FieldConf> columns, String schema) {
    }
}
