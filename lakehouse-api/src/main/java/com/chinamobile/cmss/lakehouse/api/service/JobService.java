/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.service;

import com.chinamobile.cmss.lakehouse.api.dto.JobBean;
import com.chinamobile.cmss.lakehouse.common.utils.Result;

import java.util.Map;

public interface JobService {
    /**
     * Create job
     * @param userId
     * @param jobBean
     * @return
     */
    Map<String, Object> create(String userId, JobBean jobBean);

    /**
     * Get job detail
     * @param userId
     * @param id
     * @return
     */
    Map<String, Object> get(String userId, Long id);

    /**
     * Update job
     * @param userId
     * @param id
     * @param jobBean
     * @return
     */
    Map<String, Object> update(String userId, Long id, JobBean jobBean);

    /**
     * Soft delete job
     * @param userId
     * @param id
     * @return
     */
    Map<String, Object> delete(String userId, Long id);

    /**
     * Pageable list job
     * @param userId
     * @param searchVal
     * @param pageNo
     * @param pageSize
     * @return
     */
    Result list(String userId, String searchVal, Integer pageNo, Integer pageSize);

    /**
     * Job schedule on
     * @param userId
     * @param id
     * @return
     */
    Map<String, Object> scheduleOn(String userId, Long id);

    /**
     * Job schedule on
     * @param userId
     * @param id
     * @return
     */
    Map<String, Object> scheduleOff(String userId, Long id);

    /**
     * Submit FlinkX job
     * @param id
     */
    void submitFlinkXJob(Long id);
}
