/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.config;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
@Slf4j
public class AsyncTaskConfiguration implements AsyncConfigurer {

    @Value("${async.lakehouse.core_pool_size:6}")
    private int corePoolSize;
    @Value("${async.lakehouse.max_pool_size:10}")
    private int maxPoolSize;
    @Value("${async.lakehouse.queue_capacity:50}")
    private int queueCapacity;

    @Bean(name = "LakehouseScheduleExecutor")
    public Executor lakehouseScheduleExecutor() {
        log.info("start lakehouse scheduler Executor");
        return buildExecutor(corePoolSize, maxPoolSize, queueCapacity, "lakehouse-scheduler-");
    }

    private Executor buildExecutor(int core, int max, int queue, String name) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(core);
        executor.setMaxPoolSize(max);
        executor.setQueueCapacity(queue);
        executor.setThreadNamePrefix(name);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }

    /**
     * uncatched exception handle
     */
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new AsyncUncaughtExceptionHandler() {

            @Override
            public void handleUncaughtException(Throwable ex, Method method, Object... params) {
                log.error("Uncaught Exception Occurred!!!");
                log.error("[Class...]: {}", method.getDeclaringClass().getName());
                log.error("[Method...]: {}", method.getName());
                log.error("[Params...]: {}", new Gson().toJson(params));
                log.error("[Exception details...]: ", ex);
            }
        };
    }
}
