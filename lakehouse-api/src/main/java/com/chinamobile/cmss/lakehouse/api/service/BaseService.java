/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.service;

import com.chinamobile.cmss.lakehouse.common.enums.Status;
import com.chinamobile.cmss.lakehouse.common.utils.Result;
import com.chinamobile.cmss.lakehouse.dao.entity.UserEntity;

import java.util.Map;

/**
 * base service
 */
public interface BaseService {

    /**
     * check admin
     *
     * @param user input user
     * @return ture if administrator, otherwise return false
     */
    boolean isAdmin(UserEntity user);

    /**
     * isNotAdmin
     *
     * @param loginUser login user
     * @param result    result code
     * @return true if not administrator, otherwise false
     */
    boolean isNotAdmin(UserEntity loginUser, Map<String, Object> result);

    /**
     * put message to map
     *
     * @param result       result code
     * @param status       status
     * @param statusParams status message
     */
    void putMessage(Map<String, Object> result, Status status, Object... statusParams);

    /**
     * put message to result object
     *
     * @param result       result code
     * @param status       status
     * @param statusParams status message
     */
    void putMessage(Result<Object> result, Status status, Object... statusParams);

    /**
     * check
     *
     * @param result              result
     * @param bool                bool
     * @param userNoOperationPerm status
     * @return check result
     */
    boolean check(Map<String, Object> result, boolean bool, Status userNoOperationPerm);


    /**
     * Verify that the operator has permissions
     *
     * @param operateUser  operate user
     * @param createUserId create user id
     * @return check result
     */
    boolean isAllowed(UserEntity operateUser, String createUserId);
}
