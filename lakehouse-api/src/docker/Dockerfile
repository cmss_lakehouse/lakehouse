#
# Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
# Lakehouse is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#          http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
#

# Usage:
# $ mvn clean install -DskipTests \
#       -Ddocker.hub=${your_docker_hub} \
#       -Ddocker.repo=${your_docker_repo} \
#       -Ddocker.hub.username=${your_docker_hub_username} \
#       -Ddocker.hub.password=${your_docker_hub_password} \
#       -pl lakehouse-api -am dockerfile:build dockerfile:push

FROM openjdk:8-jre-slim

ARG JAR_FILE
ADD ${JAR_FILE} /opt/

ENV LAKEHOUSE_HOME /opt/lakehouse-api
ENV LAKEHOUSE_LOG_DIR ${LAKEHOUSE_HOME}/logs
ENV LAKEHOUSE_PID_DIR ${LAKEHOUSE_HOME}/pid

ARG lakehouse_uid=19086

RUN set -ex && \
    sed -i 's/http:\/\/deb.\(.*\)/https:\/\/deb.\1/g' /etc/apt/sources.list && \
    apt-get update && \
    apt install -y bash tini libc6 libpam-modules krb5-user libnss3 procps curl && \
    curl -O -L https://mirrors.bfsu.edu.cn/apache/spark/spark-3.1.2/spark-3.1.2-bin-hadoop3.2.tgz && \
    tar zxf spark-3.1.2-bin-hadoop3.2.tgz && \
    mv spark-3.1.2-bin-hadoop3.2 /opt/spark && \
    useradd -u ${lakehouse_uid} -g root lakehouse && \
    mkdir -p ${LAKEHOUSE_HOME} ${LAKEHOUSE_LOG_DIR} ${LAKEHOUSE_PID_DIR} && \
    chmod ug+rw -R ${LAKEHOUSE_HOME} && \
    rm -rf /var/cache/apt/*

USER ${lakehouse_uid}
WORKDIR ${LAKEHOUSE_HOME}