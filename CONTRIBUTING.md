# Configure annotation processing in IntelliJ

1. Install the **Lombok** plugin (Settings → Plugins).

2. Open Annotation Processors Settings dialog box:
   Settings → Build, Execution, Deployment → Compiler → Annotation Processors

3. Select the following buttons:
   * "Enable annotation processing"
   * "Obtain processors from project classpath"
   * "Store generated sources relative to": "Module content root"

4. Set the generated source directories to be equal to the Maven directories:
   * Set "Production sources directory:" to "target/generated-sources/annotations".
   * Set "Test sources directory:" to "target/generated-test-sources/test-annotations".

5. Click OK.

## Configure Code Style

1. Install the **Checkstyle-IDEA** plugin (Settings → Plugins).

1. Open Code Style Settings dialog box:
   Settings → Editor → Code Style

2. Click on the gear symbol → Import scheme → Intellij IDEA code style XML

3. Select the file `${LAKEHOUSE_HOME}/style/checkstyle.xml`

4. Click OK.

Optionally, you also may wish to change some default IntelliJ settings, to allow the IDE to automatically reformat code for you.
* First, fix the default order import statements per our CheckStyle rules (these don't seem to be auto updated by the Checkstyle plugin at this time)
  * File → Settings → Editor → Code Style → Java → Imports
  * In the "Import Layout" section, ensure  the settings are **in this order**
    * import static all other imports
    * \<blank line\>
    * import com.chinamobile.*
    * \<blank line\>
    * import java.*
    * \<blank line\>
    * import javax.*
    * \<blank line\>
    * import all other imports
* Then, update IDEA's Javadoc settings to **not** insert "\<p\>" on blank Javadoc lines (as this seems to cause IDEA to change all our license headings improperly during bulk editing)
  * File → Settings → Editor → Code Style → Java → JavaDoc
  * UNCHECK "Generate \<p\> on empty lines"
  * Make sure "Keep empty lines" is checked
  * Click OK